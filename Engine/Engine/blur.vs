// Globals
cbuffer MatrixBuffer
{
	matrix worldMatrix;
	matrix viewMatrix;
	matrix projectionMatrix;
	matrix transformationMatrix;
};


cbuffer ScreenSizeBuffer
{
	float2 screenSize;
	float timer;
	float padding;
};


struct VertexInputType
{
    float4 position : POSITION;
    float2 tex : TEXCOORD0;
};


struct PixelInputType
{
    float4 position : SV_POSITION;
    float2 tex : TEXCOORD0;
	float2 texCoordL1 : TEXCOORD1;
	float2 texCoordL2 : TEXCOORD2;
	float2 texCoordL3 : TEXCOORD3;
	float2 texCoordR1 : TEXCOORD4;
	float2 texCoordR2 : TEXCOORD5;
	float2 texCoordR3 : TEXCOORD6;
	float2 texCoordD1 : TEXCOORD7;
	float2 texCoordD2 : TEXCOORD8;
	float2 texCoordD3 : TEXCOORD9;
	float2 texCoordU1 : TEXCOORD10;
	float2 texCoordU2 : TEXCOORD11;
	float2 texCoordU3 : TEXCOORD12;
};


PixelInputType BlurVertexShader(VertexInputType input)
{
    PixelInputType output;
	float texelWidth;
	float texelHeight;

	// Change the position vector to be 4 units for proper matrix calculations.
    input.position.w = 1.0f;

	// Calculate the position of the vertex against the world, view, and projection matrices.
    output.position = mul(input.position, worldMatrix);
	output.position = mul(output.position, transformationMatrix);
    output.position = mul(output.position, viewMatrix);
    output.position = mul(output.position, projectionMatrix);
    
	// Store the texture coordinates for the pixel shader.
	output.tex = input.tex;
    
	// Determine the floating point size of a texel for a screen with this specific width.
	texelWidth = 1.0f / screenSize.x;
	texelHeight = 1.0f / screenSize.y;

	// Create UV coordinates for the pixel and its four horizontal neighbors on either side.
	output.texCoordL1 = input.tex + float2(texelWidth * -1.0f, 0.0f);
	output.texCoordL2 = input.tex + float2(texelWidth * -2.0f, 0.0f);
	output.texCoordL3 = input.tex + float2(texelWidth * -3.0f, 0.0f);
	output.texCoordR1 = input.tex + float2(texelWidth * 1.0f, 0.0f);
	output.texCoordR2 = input.tex + float2(texelWidth * 2.0f, 0.0f);
	output.texCoordR3 = input.tex + float2(texelWidth * 3.0f, 0.0f);
	output.texCoordD1 = input.tex + float2(0, texelHeight * -1.0f);
	output.texCoordD2 = input.tex + float2(0, texelHeight * -2.0f);
	output.texCoordD3 = input.tex + float2(0, texelHeight * -3.0f);
	output.texCoordU1 = input.tex + float2(0, texelHeight * 1.0f);
	output.texCoordU2 = input.tex + float2(0, texelHeight * 2.0f);
	output.texCoordU3 = input.tex + float2(0, texelHeight * 3.0f);

    return output;
}