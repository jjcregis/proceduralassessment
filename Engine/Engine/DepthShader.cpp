#include "DepthShader.h"


DepthShader::DepthShader(ID3D11Device* device, HWND hwnd, bool useDX11) :
	Shader(device, hwnd, useDX11, L"../Engine/Depth.vs", L"../Engine/Depth.ps", "DepthVertexShader", "DepthPixelShader")
{
	D3D11_INPUT_ELEMENT_DESC polygonLayout[1];
	unsigned int numElements;

	// Create the vertex input layout description.
	// This setup needs to match the VertexType stucture in the ModelClass and in the shader.
	polygonLayout[0].SemanticName = "POSITION";
	polygonLayout[0].SemanticIndex = 0;
	polygonLayout[0].Format = DXGI_FORMAT_R32G32B32_FLOAT;
	polygonLayout[0].InputSlot = 0;
	polygonLayout[0].AlignedByteOffset = 0;
	polygonLayout[0].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	polygonLayout[0].InstanceDataStepRate = 0;

	// Get a count of the elements in the layout.
	numElements = sizeof(polygonLayout) / sizeof(polygonLayout[0]);

	// Create the vertex input layout.
	device->CreateInputLayout(polygonLayout, numElements, _vertexShaderBuffer->GetBufferPointer(), _vertexShaderBuffer->GetBufferSize(), &_layout);

	// Release the vertex shader buffer and pixel shader buffer since they are no longer needed.
	_vertexShaderBuffer->Release();
	_vertexShaderBuffer = 0;

	_pixelShaderBuffer->Release();
	_pixelShaderBuffer = 0;
}


DepthShader::~DepthShader()
{
}


bool DepthShader::Render(ID3D11DeviceContext* deviceContext, int indexCount, D3DXMATRIX worldMatrix, D3DXMATRIX viewMatrix, 
							D3DXMATRIX projectionMatrix, D3DXMATRIX transformationMatrix)
{
	bool result;

	// Set the shader parameters that it will use for rendering.
	result = SetShaderParameters(deviceContext, worldMatrix, viewMatrix, projectionMatrix, transformationMatrix);
	if(!result)
	{
		return false;
	}

	// Now render the prepared buffers with the shader.
	RenderShader(deviceContext, indexCount);

	return true;
}

bool DepthShader::SetShaderParameters(ID3D11DeviceContext* deviceContext, D3DXMATRIX worldMatrix, D3DXMATRIX viewMatrix,
										D3DXMATRIX projectionMatrix, D3DXMATRIX translationMatrix)
{
	Shader::SetShaderParameters(deviceContext, worldMatrix, viewMatrix, projectionMatrix, translationMatrix);

	return true;
}