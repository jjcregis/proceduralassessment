/*
#include "VolumeXapo.h"
#include <assert.h>

STDMETHODIMP VolumeXapo::LockForProcess(
	UINT32 InputLockedParameterCount,
	const XAPO_LOCKFORPROCESS_BUFFER_PARAMETERS  *pInputLockedParameters,
	UINT32 OutputLockedParameterCount,
	const XAPO_LOCKFORPROCESS_BUFFER_PARAMETERS  *pOutputLockedParameters
	)
{
	assert(!IsLocked());
	assert(InputLockedParameterCount == 1);
	assert(OutputLockedParameterCount == 1);
	assert(pInputLockedParameters != NULL);
	assert(pOutputLockedParameters != NULL);
	assert(pInputLockedParameters[0].pFormat != NULL);
	assert(pOutputLockedParameters[0].pFormat != NULL);


	_uChannels = pInputLockedParameters[0].pFormat->nChannels;
	_uBytesPerSample = (pInputLockedParameters[0].pFormat->wBitsPerSample >> 3);

	return CXAPOBase::LockForProcess(
		InputLockedParameterCount,
		pInputLockedParameters,
		OutputLockedParameterCount,
		pOutputLockedParameters);
}

void VolumeXapo::Process(UINT32 InputProcessParameterCount,
	const XAPO_PROCESS_BUFFER_PARAMETERS *pInputProcessParameters,
	UINT32 OutputProcessParameterCount,
	XAPO_PROCESS_BUFFER_PARAMETERS *pOutputProcessParameters,
	BOOL IsEnabled)
{
	assert(IsLocked());
	assert(InputProcessParameterCount == 1);
	assert(OutputProcessParameterCount == 1);
	assert(NULL != pInputProcessParameters);
	assert(NULL != pOutputProcessParameters);


	XAPO_BUFFER_FLAGS inFlags = pInputProcessParameters[0].BufferFlags;
	XAPO_BUFFER_FLAGS outFlags = pOutputProcessParameters[0].BufferFlags;

	// assert buffer flags are legitimate
	assert(inFlags == XAPO_BUFFER_VALID || inFlags == XAPO_BUFFER_SILENT);
	assert(outFlags == XAPO_BUFFER_VALID || outFlags == XAPO_BUFFER_SILENT);

	// check input APO_BUFFER_FLAGS
	switch (inFlags)
	{
	case XAPO_BUFFER_VALID:
	{
		void* pvSrc = pInputProcessParameters[0].pBuffer;
		assert(pvSrc != NULL);

		void* pvDst = pOutputProcessParameters[0].pBuffer;
		assert(pvDst != NULL);

		memcpy(pvDst, pvSrc, pInputProcessParameters[0].ValidFrameCount * _uChannels * _uBytesPerSample);

		//pInputProcessParameters[0].pBuffer;
		FLOAT32* __restrict pData = (FLOAT32* __restrict)pInputProcessParameters[0].pBuffer;

		//float gain = params.gain;
		for (UINT32 i = 0; i < pInputProcessParameters[0].ValidFrameCount * _uChannels; ++i)
		{
			//pData[i] *= gain;
			pData[i] *= 2.0f;
			//pData[i] = (int)pData[i];// % .01f;
			//pData[i] += 5;
		}


		break;
	}

	case XAPO_BUFFER_SILENT:
	{
		// All that needs to be done for this case is setting the
		// output buffer flag to XAPO_BUFFER_SILENT which is done below.
		break;
	}

	}

	// set destination valid frame count, and buffer flags
	pOutputProcessParameters[0].ValidFrameCount = pInputProcessParameters[0].ValidFrameCount; // set destination frame count same as source
	pOutputProcessParameters[0].BufferFlags = pInputProcessParameters[0].BufferFlags;     // set destination buffer flags same as source
}

*/