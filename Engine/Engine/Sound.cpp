#include "Sound.h"
#include "XAudio2fx.h"

Sound::Sound(HWND hwnd, WCHAR* fileName) :
	_audio(0),
	_sourceVoice(0),
	_masterVoice(0),
	_reverbEffect(0)
{
	// Create instance of XAudio2 engine
	XAudio2Create(&_audio, 0, XAUDIO2_DEFAULT_PROCESSOR);

	// Create a mastering voice
	_audio->CreateMasteringVoice(&_masterVoice);

	// Declare structures
	WAVEFORMATEXTENSIBLE wfx = { 0 };
	XAUDIO2_BUFFER buffer = { 0 };

	// Open the audio file
	HANDLE hFile = CreateFile(
		fileName,
		GENERIC_READ,
		FILE_SHARE_READ,
		NULL,
		OPEN_EXISTING,
		0,
		NULL);
	SetFilePointer(hFile, 0, NULL, FILE_BEGIN);

	// Locate the 'RIFF' chunk in the audio file and check the file type
	DWORD dwChunkSize;
	DWORD dwChunkPosition;

	//Check the file type, it should be fourccWAVE or 'XWMA'
	FindChunk(hFile, fourccRIFF, dwChunkSize, dwChunkPosition);
	DWORD filetype;
	ReadChunkData(hFile, &filetype, sizeof(DWORD), dwChunkPosition);

	// Locate the 'fmt ' chunk and copy into a WFX struct
	FindChunk(hFile, fourccFMT, dwChunkSize, dwChunkPosition);
	ReadChunkData(hFile, &wfx, dwChunkSize, dwChunkPosition);

	// Locate the 'data' chunk and read into a buffer
	// Fill out the audio data buffer with the contents of the fourccDATA chunk
	FindChunk(hFile, fourccDATA, dwChunkSize, dwChunkPosition);
	BYTE * pDataBuffer = new BYTE[dwChunkSize];
	ReadChunkData(hFile, pDataBuffer, dwChunkSize, dwChunkPosition);

	// Populate an XAudio2 buffer struct
	buffer.AudioBytes = dwChunkSize;  //buffer containing audio data
	buffer.pAudioData = pDataBuffer;  //size of the audio buffer in bytes
	buffer.Flags = XAUDIO2_END_OF_STREAM; // tell the source voice not to expect any data after this buffer
	buffer.LoopLength = buffer.PlayLength;
	buffer.LoopCount = 255;

	// Create reverb effect
	HRESULT hr = XAudio2CreateReverb(&_reverbEffect);

	// Create effect descriptor
	// (Each effect in the chain will need their own descriptor)
	XAUDIO2_EFFECT_DESCRIPTOR descriptor;
	descriptor.InitialState = true;
	descriptor.OutputChannels = 1;
	descriptor.pEffect = _reverbEffect;

	// Create the effect chain
	_effectChain.EffectCount = 1;
	_effectChain.pEffectDescriptors = &descriptor;

	// Populate the reverb effect parameters
	XAUDIO2FX_REVERB_PARAMETERS reverbParameters;
	reverbParameters.ReflectionsDelay = XAUDIO2FX_REVERB_DEFAULT_REFLECTIONS_DELAY;
	reverbParameters.ReverbDelay = XAUDIO2FX_REVERB_DEFAULT_REVERB_DELAY;
	reverbParameters.RearDelay = XAUDIO2FX_REVERB_DEFAULT_REAR_DELAY;
	reverbParameters.PositionLeft = XAUDIO2FX_REVERB_DEFAULT_POSITION;
	reverbParameters.PositionRight = XAUDIO2FX_REVERB_DEFAULT_POSITION;
	reverbParameters.PositionMatrixLeft = XAUDIO2FX_REVERB_DEFAULT_POSITION_MATRIX;
	reverbParameters.PositionMatrixRight = XAUDIO2FX_REVERB_DEFAULT_POSITION_MATRIX;
	reverbParameters.EarlyDiffusion = XAUDIO2FX_REVERB_DEFAULT_EARLY_DIFFUSION;
	reverbParameters.LateDiffusion = XAUDIO2FX_REVERB_DEFAULT_LATE_DIFFUSION;
	reverbParameters.LowEQGain = XAUDIO2FX_REVERB_DEFAULT_LOW_EQ_GAIN;
	reverbParameters.LowEQCutoff = XAUDIO2FX_REVERB_DEFAULT_LOW_EQ_CUTOFF;
	reverbParameters.HighEQGain = XAUDIO2FX_REVERB_DEFAULT_HIGH_EQ_GAIN;
	reverbParameters.HighEQCutoff = XAUDIO2FX_REVERB_DEFAULT_HIGH_EQ_CUTOFF;
	reverbParameters.RoomFilterFreq = XAUDIO2FX_REVERB_DEFAULT_ROOM_FILTER_FREQ;
	reverbParameters.RoomFilterMain = XAUDIO2FX_REVERB_DEFAULT_ROOM_FILTER_MAIN;
	reverbParameters.RoomFilterHF = XAUDIO2FX_REVERB_DEFAULT_ROOM_FILTER_HF;
	reverbParameters.ReflectionsGain = XAUDIO2FX_REVERB_DEFAULT_REFLECTIONS_GAIN;
	reverbParameters.ReverbGain = XAUDIO2FX_REVERB_DEFAULT_REVERB_GAIN;
	reverbParameters.DecayTime = XAUDIO2FX_REVERB_DEFAULT_DECAY_TIME * 10;
	reverbParameters.Density = XAUDIO2FX_REVERB_DEFAULT_DENSITY;
	reverbParameters.RoomSize = XAUDIO2FX_REVERB_DEFAULT_ROOM_SIZE;
	reverbParameters.WetDryMix = XAUDIO2FX_REVERB_DEFAULT_WET_DRY_MIX;

	// Create the source voice with the option to use audio filters
	_audio->CreateSourceVoice(&_sourceVoice, (WAVEFORMATEX*)&wfx,
		XAUDIO2_VOICE_USEFILTER, XAUDIO2_MAX_FREQ_RATIO, NULL, NULL, NULL);

	// Submit the audio buffer
	_sourceVoice->SubmitSourceBuffer(&buffer);

	// Start the voice
	_sourceVoice->Start(0, XAUDIO2_COMMIT_NOW);

	// Get the details of the source voice
	XAUDIO2_VOICE_DETAILS sourceDetails;
	_sourceVoice->GetVoiceDetails(&sourceDetails);

	// Create filters
	_noFilter.Type = LowPassFilter;
	_noFilter.Frequency = 1.0f;
	_noFilter.OneOverQ = 1.0f;

	_lowPassFilter.Type = LowPassFilter;
	_lowPassFilter.Frequency = (2 * sin(3.14159f * (308000) / sourceDetails.InputSampleRate));
	_lowPassFilter.OneOverQ = 1.0f;
	
	_highPassFilter.Type = HighPassFilter;
	_highPassFilter.Frequency = (2 * sin(3.14159f * (304000) / sourceDetails.InputSampleRate));
	_highPassFilter.OneOverQ = 1.0f;

	// Set voice effects	
	_sourceVoice->SetEffectChain(&_effectChain);
	_sourceVoice->SetEffectParameters(0, &reverbParameters, sizeof(reverbParameters));
}


Sound::~Sound()
{
	// Release the effects
	if (_reverbEffect)
	{
		_reverbEffect->Release();
		_reverbEffect = 0;
	}

	// Release the voices
	if (_masterVoice)
	{
		_masterVoice->DestroyVoice();
		_masterVoice = 0;
	}

	if (_sourceVoice)
	{
		_sourceVoice->DestroyVoice();
		_sourceVoice = 0;
	}

	// Release the XAudio2 object
	if (_audio)
	{
		_audio->Release();
		_audio = 0;
	}	
}


void Sound::SetEffect(GameBoard::GameEffect effect)
{
	// Reset all effects to start
	_sourceVoice->DisableEffect(0);
	_sourceVoice->SetFrequencyRatio(1);
	_sourceVoice->SetFilterParameters(&_noFilter);

	// Apply effects and filters
	switch (effect)
	{
	case GameBoard::Blur:
		_sourceVoice->SetFilterParameters(&_lowPassFilter);
		break;
	case GameBoard::Edge:
		_sourceVoice->SetFilterParameters(&_highPassFilter);
		break;
	case GameBoard::DoubleVision:
		// Slower, lower pitched, and with reverb
		_sourceVoice->EnableEffect(0);
		_sourceVoice->SetFrequencyRatio(.9f);
		break;
	case GameBoard::Pixelate:
		// Faster and at a higher pitch
		_sourceVoice->SetFrequencyRatio(2);
		break;
	case GameBoard::None:
	default:
		break;
	}
}


HRESULT Sound::FindChunk(HANDLE hFile, DWORD fourcc, DWORD & dwChunkSize, DWORD & dwChunkDataPosition)
{
	// Finds a chunk in a RIFF file.
	HRESULT hr = S_OK;
	if (INVALID_SET_FILE_POINTER == SetFilePointer(hFile, 0, NULL, FILE_BEGIN))
	{
		return HRESULT_FROM_WIN32(GetLastError());
	}

	DWORD dwChunkType;
	DWORD dwChunkDataSize;
	DWORD dwRIFFDataSize = 0;
	DWORD dwFileType;
	DWORD bytesRead = 0;
	DWORD dwOffset = 0;

	while (hr == S_OK)
	{
		DWORD dwRead;
		if (ReadFile(hFile, &dwChunkType, sizeof(DWORD), &dwRead, NULL) == 0)
		{
			hr = HRESULT_FROM_WIN32(GetLastError());
		}

		if (ReadFile(hFile, &dwChunkDataSize, sizeof(DWORD), &dwRead, NULL) == 0)
		{
			hr = HRESULT_FROM_WIN32(GetLastError());
		}

		switch (dwChunkType)
		{
		case fourccRIFF:
			dwRIFFDataSize = dwChunkDataSize;
			dwChunkDataSize = 4;
			if (ReadFile(hFile, &dwFileType, sizeof(DWORD), &dwRead, NULL) == 0)
			{
				hr = HRESULT_FROM_WIN32(GetLastError());
			}
			break;

		default:
			if (INVALID_SET_FILE_POINTER == SetFilePointer(hFile, dwChunkDataSize, NULL, FILE_CURRENT))
			{
				return HRESULT_FROM_WIN32(GetLastError());
			}
		}

		dwOffset += sizeof(DWORD)* 2;

		if (dwChunkType == fourcc)
		{
			dwChunkSize = dwChunkDataSize;
			dwChunkDataPosition = dwOffset;
			return S_OK;
		}

		dwOffset += dwChunkDataSize;

		if (bytesRead >= dwRIFFDataSize)
		{
			return S_FALSE;
		}
	}

	return S_OK;
}


HRESULT Sound::ReadChunkData(HANDLE hFile, void * buffer, DWORD buffersize, DWORD bufferoffset)
{
	// Reads a chunk after it has been located
	HRESULT hr = S_OK;
	if (INVALID_SET_FILE_POINTER == SetFilePointer(hFile, bufferoffset, NULL, FILE_BEGIN))
	{
		return HRESULT_FROM_WIN32(GetLastError());
	}

	DWORD dwRead;
	if (0 == ReadFile(hFile, buffer, buffersize, &dwRead, NULL))
	{
		hr = HRESULT_FROM_WIN32(GetLastError());
	}
	return hr;
}