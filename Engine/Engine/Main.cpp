#include "SystemClass.h"

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, PSTR pScmdline, int iCmdshow)
{
	SystemClass* System;
	
	// Create and initialize the system object.
	System = new SystemClass();

	// Run the application
	System->Run();

	// Shutdown and release the system object.
	delete System;
	System = 0;

	return 0;
}