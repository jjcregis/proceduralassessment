#ifndef _SOUND_H_
#define _SOUND_H_

// Defines
#define XAUDIO2_HELPER_FUNCTIONS
#define fourccRIFF 'FFIR'
#define fourccDATA 'atad'
#define fourccFMT ' tmf'
#define fourccWAVE 'EVAW'
#define fourccXWMA 'AMWX'
#define fourccDPDS 'sdpd'

// Include
#include <windows.h>
#include <xaudio2.h>
#include "GameBoard.h"

class Sound
{
public:
	Sound(HWND, WCHAR*);
	~Sound();

	void SetEffect(GameBoard::GameEffect);

private:
	HRESULT FindChunk(HANDLE hFile, DWORD fourcc, DWORD & dwChunkSize, DWORD & dwChunkDataPosition);
	HRESULT ReadChunkData(HANDLE hFile, void * buffer, DWORD buffersize, DWORD bufferoffset);

	IXAudio2* _audio;
	IXAudio2SourceVoice* _sourceVoice;
	IXAudio2MasteringVoice* _masterVoice;
	XAUDIO2_EFFECT_CHAIN _effectChain;

	IUnknown * _reverbEffect;
	
	XAUDIO2_FILTER_PARAMETERS _noFilter;
	XAUDIO2_FILTER_PARAMETERS _lowPassFilter;
	XAUDIO2_FILTER_PARAMETERS _highPassFilter;
};

#endif