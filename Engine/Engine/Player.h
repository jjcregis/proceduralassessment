#ifndef _PLAYER_H_
#define _PLAYER_H_

#include <d3d11.h>
#include <d3dx10math.h>
#include "Model.h"

class Input;
class TileGrid;

class Player : public Model
{
public:
	Player(ID3D11Device*, Input*, TileGrid*, char*, WCHAR*, WCHAR* secondaryFilename = NULL, D3DXVECTOR3 position = D3DXVECTOR3(0, 0, 0), float scale = 1);
	~Player();

	TileGrid* GetGrid() const;
	D3DXVECTOR3 GetGridPosition() const;

	void Frame(float);
private:
	Input* _input;
	TileGrid* _grid;
	D3DXVECTOR3 _gridPosition;
};

#endif