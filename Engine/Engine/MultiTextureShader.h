#ifndef _MULTITEXTURESHADER_H_
#define _MULTITEXTURESHADER_H_

// Includes
#include <d3d11.h>
#include <d3dx10math.h>
#include <d3dx11async.h>
#include <fstream>
#include "Shader.h"
using namespace std;

class MultiTextureShader : public Shader
{

public:
	MultiTextureShader(ID3D11Device*, HWND, bool);
	~MultiTextureShader();
	bool Render(ID3D11DeviceContext*, int, D3DXMATRIX, D3DXMATRIX, D3DXMATRIX, D3DXMATRIX, ID3D11ShaderResourceView**);

private:
	bool SetShaderParameters(ID3D11DeviceContext*, D3DXMATRIX, D3DXMATRIX, D3DXMATRIX, D3DXMATRIX, ID3D11ShaderResourceView**);
};

#endif