#ifndef _MODEL_H_
#define _MODEL_H_

// Includes
#include <d3d11.h>
#include <d3dx10math.h>
#include <fstream>
#include "TextureArray.h"
using namespace std;

class Model
{
private:
	struct VertexType
	{
		D3DXVECTOR3 position;
		D3DXVECTOR2 texture;
		D3DXVECTOR3 normal;
		D3DXVECTOR3 tangent;
		D3DXVECTOR3 binormal;
	};

	struct ModelType
	{
		float x, y, z;
		float tu, tv;
		float nx, ny, nz;
		float tx, ty, tz;
		float bx, by, bz;
	};

		struct TempVertexType
	{
		float x, y, z;
		float tu, tv;
		float nx, ny, nz;
	};

	struct VectorType
	{
		float x, y, z;
	};

public:
	Model(ID3D11Device*, char*, WCHAR*, WCHAR* secondaryFilename = NULL, D3DXVECTOR3 position = D3DXVECTOR3(0, 0, 0), float scale = 1);
	Model(const Model&);
	~Model();

	void Render(ID3D11DeviceContext*);
	void CalculateModelVectors();
	void Frame(float);

	int GetIndexCount();
	D3DXMATRIX GetTranslationMatrix();
	ID3D11ShaderResourceView** GetTextureArray();
	ID3D11ShaderResourceView* GetPrimaryTexture();
	ID3D11ShaderResourceView* GetSecondaryTexture();

	void AddTranslation(float, float, float);
	void AddScale(float, float, float);
	void AddRotation(float, float, float);

	D3DXVECTOR3 GetTranslation();
	D3DXVECTOR3 GetScale();
	D3DXVECTOR3 GetRotation();

private:
	bool InitializeBuffers(ID3D11Device*);
	void ShutdownBuffers();
	void RenderBuffers(ID3D11DeviceContext*);
	
	void LoadTextures(ID3D11Device*, WCHAR*, WCHAR*);
	void ReleaseTextures();

	bool LoadModel(char*);
	void ReleaseModel();
	
	void CalculateTangentBinormal(TempVertexType, TempVertexType, TempVertexType, VectorType&, VectorType&);
	void CalculateNormal(VectorType, VectorType, VectorType&);

protected:
	ID3D11Buffer *_vertexBuffer, *_indexBuffer;
	int _vertexCount, _indexCount;
	ModelType* _model;
	TextureArray* _textureArray;
	
	D3DXVECTOR3 _position;
	D3DXVECTOR3 _rotation;
	D3DXVECTOR3 _scale;
};

#endif