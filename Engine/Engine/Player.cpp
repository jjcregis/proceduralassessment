#include "Player.h"
#include "Input.h"
#include "TileGrid.h"


Player::Player(ID3D11Device* device, Input* input, TileGrid* grid, char* modelFilename, WCHAR* primaryFilename, WCHAR* secondaryFilename, D3DXVECTOR3 position, float scale) :
	Model(device, modelFilename, primaryFilename, secondaryFilename, position, scale),
	_input(input),
	_grid(grid),
	_gridPosition(D3DXVECTOR3(0, 0, 0))
{
}


Player::~Player()
{

}


TileGrid* Player::GetGrid() const
{
	return _grid;
}


D3DXVECTOR3 Player::GetGridPosition() const
{
	return _gridPosition;
}


void Player::Frame(float frameTime)
{
	// Update the player's grid position
	D3DXVECTOR3 playerPosition = GetTranslation();
	_gridPosition = D3DXVECTOR3(playerPosition.x + _grid->WIDTH / 2, playerPosition.y + _grid->HEIGHT / 2, 0);

	// Slowly rotate the cube
	AddRotation(frameTime * .001f, frameTime * .0006f, frameTime * .0002f);
}