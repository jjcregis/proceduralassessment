#ifndef _TEXTURE_H_
#define _TEXTURE_H_

// Includes
#include <d3d11.h>
#include <d3dx11tex.h>

class Texture
{
public:
	Texture(ID3D11Device*, WCHAR*);
	Texture(const Texture&);
	~Texture();

	void SetTexture(ID3D11Device*, WCHAR*);
	ID3D11ShaderResourceView* GetTexture();

private:
	ID3D11ShaderResourceView* _texture;
};

#endif