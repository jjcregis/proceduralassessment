#include "Graphics.h"

Graphics::Graphics(Camera* camera, Input* input, int screenWidth, int screenHeight, HWND hwnd) :
	_d3d(0),
	_light(0),
	_renderTexture(0),
	_debugWindow(0),
	_fpsText(0),
	_retryText(0),
	_shaderManager(0),
	_skyBox(0),
	_board(0),
	_camera(camera),
	_input(input)
{
	bool usingDX11;
	D3DXMATRIX baseViewMatrix;

	// Create and initialize Direct3D.
	_d3d = new D3DCore(screenWidth, screenHeight, VSYNC_ENABLED, hwnd, FULL_SCREEN, SCREEN_DEPTH, SCREEN_NEAR, usingDX11);

	// Initialize a base view matrix with the camera for 2D user interface rendering.
	_camera->SetPosition(0.0f, 0.0f, -1.0f);
	_camera->Render();
	_camera->GetViewMatrix(baseViewMatrix);

	// Set the initial position of the camera.
	_camera->SetPosition(0, 0, -42);

	// Create and initialize the light object.
	_light = new Light();
	_light->SetAmbientColor(.3f, .3f, .3f, 1);
	_light->SetDiffuseColor(.8f, .8f, .8f, 1);
	_light->SetDirection(0, 0, 1);

	// Create and initialize the shader manager and all shaders in it.
	_shaderManager = new ShaderManager(_d3d->GetDevice(), hwnd, usingDX11);

	// Create and initialize the skybox
	_skyBox = new Model(_d3d->GetDevice(), "../Engine/data/sphere.txt", L"../Engine/data/galaxy2.png", NULL, D3DXVECTOR3(0, 0, 0), -40);

	// Create and initialize the game board
	_board = new GameBoard(_d3d->GetDevice(), input);

	// Create and initialize the FPS text object.
	_fpsText = new Text(_d3d->GetDevice(), _d3d->GetDeviceContext(), hwnd, screenWidth, screenHeight, baseViewMatrix, usingDX11);

	// Create and initialize the retry text object.
	_retryText = new Text(_d3d->GetDevice(), _d3d->GetDeviceContext(), hwnd, screenWidth, screenHeight, baseViewMatrix, usingDX11);
	_retryText->UpdateText("Press R to restart", 20, 40, 1, 1, 1, _d3d->GetDeviceContext());

	// Create and initialize the render to texture object.
	_renderTexture = new RenderTexture(_d3d->GetDevice(), screenWidth, screenHeight);

	// Create and initialize the debug window.
	_debugWindow = new DebugWindow(_d3d->GetDevice(), screenWidth, screenHeight, 800, 600);
}


Graphics::Graphics(const Graphics& other)
{
}


Graphics::~Graphics()
{
	// Release the light object.
	if (_light)
	{
		delete _light;
		_light = 0;
	}

	// Release the skybox
	if (_skyBox)
	{
		delete _skyBox;
		_skyBox = 0;
	}

	// Release the fps text object.
	if (_fpsText)
	{
		delete _fpsText;
		_fpsText = 0;
	}

	// Release the retry text object.
	if (_retryText)
	{
		delete _retryText;
		_retryText = 0;
	}

	// Release the debug window object.
	if (_debugWindow)
	{
		delete _debugWindow;
		_debugWindow = 0;
	}

	// Release the game board
	if (_board)
	{
		delete _board;
		_board = 0;
	}

	// Release the render to texture object.
	if (_renderTexture)
	{
		delete _renderTexture;
		_renderTexture = 0;
	}

	// Release the D3D object.
	if (_d3d)
	{
		delete _d3d;
		_d3d = 0;
	}
}


bool Graphics::Frame(int fps, float frameTime, float totalTime, GameBoard::GameEffect& effect)
{
	bool result;

	// Update the game board
	_board->Frame(frameTime, _d3d->GetDeviceContext());

	// Update the camera's location to follow the player
	D3DXVECTOR3 playerPosition = _board->GetPlayer()->GetTranslation();
	_camera->SetPosition(playerPosition.x, playerPosition.y, playerPosition.z - 6);

	// Update the frames per second for the FPS display text.
	result = _fpsText->SetFps(fps, _d3d->GetDeviceContext());
	if(!result)
		return false;

	// Update the light direction
	_light->Frame(totalTime);

	// Rotate the skybox
	_skyBox->AddRotation(0, 0, .001f);

	// Render the graphics scene.
	result = Render();
	if(!result)
		return false;

	// Get the current GameEffect to send to other systems
	effect = _board->GetEffect();

	return true;
}


bool Graphics::Render()
{
	bool result;

	// Render the entire scene to the texture first.
	result = RenderToTexture();
	if (!result)
	{
		return false;
	}

	// Clear the buffers to begin the scene.
	_d3d->BeginScene(0/255, 0/255, 32/255, 1);

	// Turn off the Z buffer to begin all 2D rendering.
	_d3d->TurnZBufferOff();

	// Get the world, view, and ortho matrices from the camera and d3d objects.
	_d3d->GetWorldMatrix(worldMatrix);
	_camera->GetViewMatrix(viewMatrix);
	_d3d->GetOrthoMatrix(orthoMatrix);

	// Put the debug window vertex and index buffers on the graphics pipeline to prepare them for drawing.
	result = _debugWindow->Render(_d3d->GetDeviceContext(), 0, 0);
	if (!result)
	{
		return false;
	}

	// Render the debug window using the texture shader.
	D3DXMATRIX identity;
	D3DXMatrixIdentity(&identity);

	switch (_board->GetEffect())
	{
	case GameBoard::Blur:
		result = _shaderManager->RenderBlurShader(_d3d->GetDeviceContext(), _debugWindow->GetIndexCount(), worldMatrix, viewMatrix,
			orthoMatrix, _camera->GetTransformationMatrix(), _renderTexture->GetShaderResourceView(), D3DXVECTOR2(800, 600), _board->GetTimer());
		break;
	case GameBoard::Edge:
		result = _shaderManager->RenderEdgeShader(_d3d->GetDeviceContext(), _debugWindow->GetIndexCount(), worldMatrix, viewMatrix,
			orthoMatrix, _camera->GetTransformationMatrix(), _renderTexture->GetShaderResourceView(), D3DXVECTOR2(800, 600), _board->GetTimer());
		break;
	case GameBoard::DoubleVision:
		result = _shaderManager->RenderDoubleVisionShader(_d3d->GetDeviceContext(), _debugWindow->GetIndexCount(), worldMatrix, viewMatrix,
			orthoMatrix, _camera->GetTransformationMatrix(), _renderTexture->GetShaderResourceView(), D3DXVECTOR2(800, 600), _board->GetTimer());
		break;
	case GameBoard::Pixelate:
		result = _shaderManager->RenderPixelateShader(_d3d->GetDeviceContext(), _debugWindow->GetIndexCount(), worldMatrix, viewMatrix,
			orthoMatrix, _camera->GetTransformationMatrix(), _renderTexture->GetShaderResourceView(), D3DXVECTOR2(800, 600), _board->GetTimer());
		break;
	case GameBoard::Split:
		result = _shaderManager->RenderSplitShader(_d3d->GetDeviceContext(), _debugWindow->GetIndexCount(), worldMatrix, viewMatrix,
			orthoMatrix, _camera->GetTransformationMatrix(), _renderTexture->GetShaderResourceView(), D3DXVECTOR2(800, 600), _board->GetTimer());
		break;
	case GameBoard::None:
	default:
		result = _shaderManager->RenderTextureShader(_d3d->GetDeviceContext(), _debugWindow->GetIndexCount(), worldMatrix, viewMatrix,
			orthoMatrix, _camera->GetTransformationMatrix(), _renderTexture->GetShaderResourceView());
		break;
	}

	if (!result)
	{
		return false;
	}

	// Turn on alpha blending for HUD rendering
	_d3d->TurnOnAlphaBlending();

	// Render the FPS string.
	result = _fpsText->Render(_d3d->GetDeviceContext(), worldMatrix, orthoMatrix);
	if (!result)
		return false;

	// Render the retry text.
	result = _retryText->Render(_d3d->GetDeviceContext(), worldMatrix, orthoMatrix);
	if (!result)
		return false;

	// Turn alpha blending off
	_d3d->TurnOffAlphaBlending();

	// Turn the Z buffer back on now that all 2D rendering has completed.
	_d3d->TurnZBufferOn();

	// Present the rendered scene to the screen.
	_d3d->EndScene();

	return true;
}


D3DCore* Graphics::GetD3D()
{
	return _d3d;
}


void Graphics::Reset()
{
	_board->Reset();
}


bool Graphics::RenderToTexture()
{
	bool result;

	// Set the render target to be the render to texture.
	_renderTexture->SetRenderTarget(_d3d->GetDeviceContext(), _d3d->GetDepthStencilView());

	// Clear the render to texture.
	_renderTexture->ClearRenderTarget(_d3d->GetDeviceContext(), _d3d->GetDepthStencilView(), 0.0f, 0.0f, 1.0f, 1.0f);

	// Render the scene now and it will draw to the render to texture instead of the back buffer.
	result = RenderScene();
	if (!result)
	{
		return false;
	}

	// Reset the render target back to the original back buffer and not the render to texture anymore.
	_d3d->SetBackBufferRenderTarget();

	return true;
}


bool Graphics::RenderScene()
{
	bool result;

	// Generate the view matrix based on the camera's position.
	_camera->Render();

	// Get the world, view, projection and orthographic matrices from the camera and d3d objects.
	_camera->GetViewMatrix(viewMatrix);
	_d3d->GetWorldMatrix(worldMatrix);
	_d3d->GetProjectionMatrix(projectionMatrix);
	_d3d->GetOrthoMatrix(orthoMatrix);

	// Render the skybox
	_skyBox->Render(_d3d->GetDeviceContext());
	result = _shaderManager->RenderTextureShader(_d3d->GetDeviceContext(), _skyBox->GetIndexCount(), worldMatrix, viewMatrix, projectionMatrix,
		_skyBox->GetTranslationMatrix(), _skyBox->GetPrimaryTexture());
	if (!result)
		return false;

	// Render the player
	Player* player = _board->GetPlayer();
	player->Render(_d3d->GetDeviceContext());
	_shaderManager->RenderTextureShader(_d3d->GetDeviceContext(), player->GetIndexCount(), worldMatrix, viewMatrix, projectionMatrix,
		player->GetTranslationMatrix(), player->GetPrimaryTexture());

	// Render the grid
	TileGrid* grid = _board->GetTileGrid();
	_board->GetTileGrid()->Render(_d3d->GetDeviceContext());
	Tile* tile;
	for (int i = 0; i < grid->WIDTH * grid->HEIGHT; i++)
	{
		tile = grid->GetTile(i);
		if (tile && tile->GetType() != 2)
		{
			_shaderManager->RenderTextureShader(_d3d->GetDeviceContext(), tile->GetIndexCount(), worldMatrix, viewMatrix, projectionMatrix,
				tile->GetTranslationMatrix(), tile->GetPrimaryTexture());
		}
	}

	// Turn on alpha blending for particle rendering
	_d3d->TurnOnAlphaBlending();

	// Render the particle system pickup
	ParticleSystem* pickup = _board->GetParticleSystem();
	pickup->Render(_d3d->GetDeviceContext());
	result = _shaderManager->RenderParticleShader(_d3d->GetDeviceContext(), pickup->GetIndexCount(), worldMatrix, viewMatrix, projectionMatrix,
		pickup->GetTranslationMatrix(), pickup->GetTexture());
	if (!result)
		return false;

	// Turn alpha blending back off
	_d3d->TurnOffAlphaBlending();

	return true;
}