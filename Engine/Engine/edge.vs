// Globals
cbuffer MatrixBuffer
{
	matrix worldMatrix;
	matrix viewMatrix;
	matrix projectionMatrix;
	matrix transformationMatrix;
};

cbuffer ScreenSizeBuffer
{
	float2 screenSize;
	float timer;
	float padding;
};


// Types
struct VertexInputType
{
    float4 position : POSITION;
    float2 tex : TEXCOORD0;
};

struct PixelInputType
{
    float4 position : SV_POSITION;
	float2 texCoord11 : TEXCOORD0;
	float2 texCoord12 : TEXCOORD1;
	float2 texCoord13 : TEXCOORD2;
	float2 texCoord21 : TEXCOORD3;
	float2 texCoord22 : TEXCOORD4;
	float2 texCoord23 : TEXCOORD5;
	float2 texCoord31 : TEXCOORD6;
	float2 texCoord32 : TEXCOORD7;
	float2 texCoord33 : TEXCOORD8;
};


PixelInputType EdgeVertexShader(VertexInputType input)
{
    PixelInputType output;
	float texelWidth;
	float texelHeight;

	// Change the position vector to be 4 units for proper matrix calculations.
    input.position.w = 1.0f;

	// Calculate the position of the vertex against the world, view, and projection matrices.
    output.position = mul(input.position, worldMatrix);
	output.position = mul(output.position, transformationMatrix);
    output.position = mul(output.position, viewMatrix);
    output.position = mul(output.position, projectionMatrix);
    
	// Determine the floating point size of a texel for a screen with this specific width.
	texelWidth = 1.0f / screenSize.x;
	texelHeight = 1.0f / screenSize.y;

	// Create UV coordinates for the pixel and its four horizontal neighbors on either side.
	output.texCoord11 = input.tex + float2(-texelWidth, -texelWidth);
	output.texCoord12 = input.tex + float2(0, -texelWidth);
	output.texCoord13 = input.tex + float2(texelWidth, -texelWidth);
	output.texCoord21 = input.tex + float2(-texelWidth, 0);
	output.texCoord22 = input.tex;
	output.texCoord23 = input.tex + float2(texelWidth, 0);
	output.texCoord31 = input.tex + float2(-texelWidth, texelHeight);
	output.texCoord32 = input.tex + float2(0, texelHeight);
	output.texCoord33 = input.tex + float2(texelWidth, texelHeight);

    return output;
}