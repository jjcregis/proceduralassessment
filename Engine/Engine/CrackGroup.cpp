#include "CrackGroup.h"

CrackGroup::CrackGroup(ID3D11Device* device, float initialRotation) :
	_device(device),
	_lSystem(0),
	_crackList(0)
{
	_lSystem = new CrackLSystem();
	_crackList = new vector<Crack*>();
	
	_lSystem->Generate(this, 1, 0, 0, 45 * 3.14159f / 180);
}


CrackGroup::~CrackGroup()
{
	// Delete the cracks in the list
	for (int i = 0; i < _crackList->size(); i++)
	{
		delete _crackList->at(i);
	}

	// Delete the list itself
	delete _crackList;

	// Delete the L-System
	delete _lSystem;
}


vector<Crack*>* CrackGroup::GetCracks()
{
	return _crackList;
}


ID3D11Device* CrackGroup::GetDevice()
{
	return _device;
}