#ifndef _SHADER_H_
#define _SHADER_H_

// Includes
#include <d3d11.h>
#include <d3dx10math.h>
#include <d3dx11async.h>
#include <fstream>
#include "ShaderBufferTypes.h"

using namespace std;

class Shader
{
public:
	Shader(ID3D11Device*, HWND, bool, WCHAR*, WCHAR*, char*, char*);
	virtual ~Shader();
	bool Render(ID3D11DeviceContext*, int, D3DXMATRIX, D3DXMATRIX, D3DXMATRIX);

protected:
	void OutputShaderErrorMessage(ID3D10Blob*, HWND, WCHAR*);
	bool SetShaderParameters(ID3D11DeviceContext*, D3DXMATRIX, D3DXMATRIX, D3DXMATRIX, D3DXMATRIX);
	void RenderShader(ID3D11DeviceContext*, int);
	void ReleaseResource(IUnknown*);

protected:
	WCHAR* _vertexShaderFilename;
	WCHAR* _pixelShaderFilename;
	char* _vertexShaderName;
	char* _pixelShaderName;

	ID3D11VertexShader* _vertexShader;
	ID3D11PixelShader* _pixelShader;
	ID3D11InputLayout* _layout;
	ID3D11SamplerState* _samplerState;
	ID3D11Buffer* _matrixBuffer;

	ID3D10Blob* _vertexShaderBuffer;
	ID3D10Blob* _pixelShaderBuffer;
};

#endif