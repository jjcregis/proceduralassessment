#include "ShaderManager.h"

ShaderManager::ShaderManager(ID3D11Device* device, HWND hwnd, bool useDX11) :
	_lightShader(0),
	_textureShader(0),
	_multiTextureShader(0),
	_bumpMapShader(0),
	_particleShader(0),
	_depthShader(0),
	_blurShader(0),
	_edgeShader(0),
	_doubleVisionShader(0),
	_pixelateShader(0),
	_splitShader(0)
{
	// Create and initialize the light shader.
	_lightShader = new LightShader(device, hwnd, useDX11);

	// Create and initialize the texture shader.
	_textureShader = new TextureShader(device, hwnd, useDX11);

	// Create and initialize the multi-texture shader.
	_multiTextureShader = new MultiTextureShader(device, hwnd, useDX11);

	// Create and initialize the bump map shader.
	_bumpMapShader = new BumpMapShader(device, hwnd, useDX11);

	// Create and initialize the particle shader.
	_particleShader = new ParticleShader(device, hwnd, useDX11);

	// Create and initialize the depth shader.
	_depthShader = new DepthShader(device, hwnd, useDX11);

	// Create and initialize the blur shader.
	_blurShader = new PostProcessShader(device, hwnd, useDX11, L"../Engine/blur.vs", L"../Engine/blur.ps", "BlurVertexShader", "BlurPixelShader");
	
	// Create and initialize the edge shader.
	_edgeShader = new PostProcessShader(device, hwnd, useDX11, L"../Engine/edge.vs", L"../Engine/edge.ps", "EdgeVertexShader", "EdgePixelShader");

	// Create and initialize the double vision shader.
	_doubleVisionShader = new PostProcessShader(device, hwnd, useDX11, L"../Engine/doublevision.vs", L"../Engine/doublevision.ps", "DoubleVisionVertexShader", "DoubleVisionPixelShader");

	// Create and initialize the pixelate shader.
	_pixelateShader = new PostProcessShader(device, hwnd, useDX11, L"../Engine/pixelate.vs", L"../Engine/pixelate.ps", "PixelateVertexShader", "PixelatePixelShader");

	// Create and initialize the split shader.
	_splitShader = new PostProcessShader(device, hwnd, useDX11, L"../Engine/split.vs", L"../Engine/split.ps", "SplitVertexShader", "SplitPixelShader");

}


ShaderManager::ShaderManager(const ShaderManager& other)
{
}


ShaderManager::~ShaderManager()
{
	// Release the light shader.
	if (_lightShader)
	{
		delete _lightShader;
		_lightShader = 0;
	}

	// Release the texture shader.
	if (_textureShader)
	{
		delete _textureShader;
		_textureShader = 0;
	}

	// Release the multi-texture shader.
	if (_multiTextureShader)
	{
		delete _multiTextureShader;
		_multiTextureShader = 0;
	}

	// Release the bump map shader.
	if (_bumpMapShader)
	{
		delete _bumpMapShader;
		_bumpMapShader = 0;
	}

	// Release the particle shader.
	if (_particleShader)
	{
		delete _particleShader;
		_particleShader = 0;
	}

	// Release the depth shader.
	if (_depthShader)
	{
		delete _depthShader;
		_depthShader = 0;
	}

	// Release the blur shader.
	if (_blurShader)
	{
		delete _blurShader;
		_blurShader = 0;
	}

	// Release the edge shader.
	if (_edgeShader)
	{
		delete _edgeShader;
		_edgeShader = 0;
	}

	// Release the double vision shader.
	if (_doubleVisionShader)
	{
		delete _doubleVisionShader;
		_doubleVisionShader = 0;
	}

	// Release the pixelate shader.
	if (_pixelateShader)
	{
		delete _pixelateShader;
		_pixelateShader = 0;
	}

	// Release the split shader.
	if (_splitShader)
	{
		delete _splitShader;
		_splitShader = 0;
	}
}


bool ShaderManager::RenderLightShader(ID3D11DeviceContext* deviceContext, int indexCount, D3DXMATRIX worldMatrix, D3DXMATRIX viewMatrix, 
										D3DXMATRIX projectionMatrix, D3DXMATRIX translationMatrix, ID3D11ShaderResourceView* texture,
										D3DXVECTOR3 lightDirection, D3DXVECTOR4 ambientColor, D3DXVECTOR4 diffuseColor)
{
	// Render the model using the light shader.
	return _lightShader->Render(deviceContext, indexCount, worldMatrix, viewMatrix, projectionMatrix, translationMatrix, texture, lightDirection, ambientColor, diffuseColor);
}


bool ShaderManager::RenderTextureShader(ID3D11DeviceContext* device, int indexCount, D3DXMATRIX worldMatrix, D3DXMATRIX viewMatrix,
										D3DXMATRIX projectionMatrix, D3DXMATRIX translationMatrix, ID3D11ShaderResourceView* texture)
{
	// Render the model using the texture shader.
	return _textureShader->Render(device, indexCount, worldMatrix, viewMatrix, projectionMatrix, translationMatrix, texture);
}


bool ShaderManager::RenderMultiTextureShader(ID3D11DeviceContext* deviceContext, int indexCount, D3DXMATRIX worldMatrix, 
												D3DXMATRIX viewMatrix, D3DXMATRIX projectionMatrix, D3DXMATRIX translationMatrix,
												ID3D11ShaderResourceView** textureArray)
{
	// Render the model using the multi-texture shader
	return _multiTextureShader->Render(deviceContext, indexCount, worldMatrix, viewMatrix, projectionMatrix, translationMatrix, textureArray);
}


bool ShaderManager::RenderBumpMapShader(ID3D11DeviceContext* deviceContext, int indexCount, D3DXMATRIX worldMatrix, D3DXMATRIX viewMatrix,
										D3DXMATRIX projectionMatrix, D3DXMATRIX translationMatrix, ID3D11ShaderResourceView** textureArray,
										D3DXVECTOR3 lightDirection, D3DXVECTOR4 diffuse, D3DXVECTOR4 ambient)
{
	// Render the model using the bump map shader.
	return _bumpMapShader->Render(deviceContext, indexCount, worldMatrix, viewMatrix, projectionMatrix, translationMatrix, textureArray, lightDirection, diffuse, ambient);
}


bool ShaderManager::RenderParticleShader(ID3D11DeviceContext* deviceContext, int indexCount, D3DXMATRIX worldMatrix, D3DXMATRIX viewMatrix, 
											D3DXMATRIX projectionMatrix, D3DXMATRIX translationMatrix, ID3D11ShaderResourceView* texture)
{
	// Render the model using the particle shader.
	return _particleShader->Render(deviceContext, indexCount, worldMatrix, viewMatrix, projectionMatrix, translationMatrix, texture);
}

bool ShaderManager::RenderDepthShader(ID3D11DeviceContext* deviceContext, int indexCount, D3DXMATRIX worldMatrix, D3DXMATRIX viewMatrix,
										D3DXMATRIX projectionMatrix, D3DXMATRIX transformationMatrix)
{
	// Render the model using the depth shader.
	return _depthShader->Render(deviceContext, indexCount, worldMatrix, viewMatrix, projectionMatrix, transformationMatrix);
}


bool ShaderManager::RenderBlurShader(ID3D11DeviceContext* deviceContext, int indexCount, D3DXMATRIX worldMatrix, D3DXMATRIX viewMatrix, D3DXMATRIX projectionMatrix,
	D3DXMATRIX transformationMatrix, ID3D11ShaderResourceView* texture, D3DXVECTOR2 screenSize, float timer)
{
	// Render the model using the blur shader.
	return _blurShader->Render(deviceContext, indexCount, worldMatrix, viewMatrix, projectionMatrix, transformationMatrix, texture, screenSize, timer);
}


bool ShaderManager::RenderEdgeShader(ID3D11DeviceContext* deviceContext, int indexCount, D3DXMATRIX worldMatrix, D3DXMATRIX viewMatrix, D3DXMATRIX projectionMatrix,
	D3DXMATRIX transformationMatrix, ID3D11ShaderResourceView* texture, D3DXVECTOR2 screenSize, float timer)
{
	// Render the model using the edge shader.
	return _edgeShader->Render(deviceContext, indexCount, worldMatrix, viewMatrix, projectionMatrix, transformationMatrix, texture, screenSize, timer);
}


bool ShaderManager::RenderDoubleVisionShader(ID3D11DeviceContext* deviceContext, int indexCount, D3DXMATRIX worldMatrix, D3DXMATRIX viewMatrix, D3DXMATRIX projectionMatrix,
	D3DXMATRIX transformationMatrix, ID3D11ShaderResourceView* texture, D3DXVECTOR2 screenSize, float timer)
{
	// Render the model using the double vision shader.
	return _doubleVisionShader->Render(deviceContext, indexCount, worldMatrix, viewMatrix, projectionMatrix, transformationMatrix, texture, screenSize, timer);
}


bool ShaderManager::RenderPixelateShader(ID3D11DeviceContext* deviceContext, int indexCount, D3DXMATRIX worldMatrix, D3DXMATRIX viewMatrix, D3DXMATRIX projectionMatrix,
	D3DXMATRIX transformationMatrix, ID3D11ShaderResourceView* texture, D3DXVECTOR2 screenSize, float timer)
{
	// Render the model using the pixelate shader.
	return _pixelateShader->Render(deviceContext, indexCount, worldMatrix, viewMatrix, projectionMatrix, transformationMatrix, texture, screenSize, timer);
}


bool ShaderManager::RenderSplitShader(ID3D11DeviceContext* deviceContext, int indexCount, D3DXMATRIX worldMatrix, D3DXMATRIX viewMatrix, D3DXMATRIX projectionMatrix,
	D3DXMATRIX transformationMatrix, ID3D11ShaderResourceView* texture, D3DXVECTOR2 screenSize, float timer)
{
	// Render the model using the split shader.
	return _splitShader->Render(deviceContext, indexCount, worldMatrix, viewMatrix, projectionMatrix, transformationMatrix, texture, screenSize, timer);
}