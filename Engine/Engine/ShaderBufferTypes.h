#ifndef _SHADERBUFFERTYPES_H_
#define _SHADERBUFFERTYPES_H_

#include <d3d11.h>
#include <d3dx10math.h>

struct MatrixBufferType
{
	D3DXMATRIX world;
	D3DXMATRIX view;
	D3DXMATRIX projection;
	D3DXMATRIX translation;
};

struct LightBufferType
{
	D3DXVECTOR4 ambientColor;
	D3DXVECTOR4 diffuseColor;
	D3DXVECTOR3 lightDirection;
	float padding;
};

struct PixelBufferType
{
	D3DXVECTOR4 pixelColor;
};

struct ScreenSizeBufferType
{
	D3DXVECTOR2 screenSize;
	float timer;
	float padding;
};

#endif