#include "GameBoard.h"
#include "Utils.h"

GameBoard::GameBoard(ID3D11Device* device, Input* input) :
	_input(input),
	_effect(None),
	_timeLimit(10000),
	_timer(10000)
{
	// Create and initialize the tile grid
	_grid = new TileGrid(device);
	D3DXVECTOR3 goalPosition;
	_grid->Build(_grid->WIDTH / 6, _grid->HEIGHT / 6, 0, goalPosition);

	// Create and initialize the particle system.
	_pickup = new ParticleSystem(device, L"../Engine/data/Circle.dds", goalPosition);

	// Create the Player
	_player = new Player(device, _input, _grid, "../Engine/data/cube.txt", L"../Engine/data/Player.png", NULL, D3DXVECTOR3(0, 0, -1), .33f);
}


GameBoard::~GameBoard()
{
	// Release the grid
	if (_grid)
	{
		delete _grid;
		_grid = 0;
	}

	// Release the player.
	if (_player)
	{
		delete _player;
		_player = 0;
	}

	// Release the particle system object.
	if (_pickup)
	{
		delete _pickup;
		_pickup = 0;
	}
}


void GameBoard::Frame(float frameTime, ID3D11DeviceContext* deviceContext)
{
	
	// Decrease time remaining
	_timer -= frameTime;

	// Stop playing if the timer has elapsed
	if (_timer <= 0)
	{
		_effect = Split;
		return;
	}
	
	// Handle input
	_input->Frame(_player, frameTime);

	// Update the player
	_player->Frame(frameTime);

	// Run the frame processing for the particle system.
	_pickup->Frame(frameTime, deviceContext);

	// Update the tile grid
	_grid->Frame(frameTime);

	// If the player is close enough to the particle system:
	//	1) Rebuild the map
	//	2) Show a post-processing effect
	//	3) Move the particle system
	D3DXVECTOR3 pickupPosition = _pickup->GetPosition();
	D3DXVECTOR3 playerToPickupVector = pickupPosition - _player->GetTranslation();
	if (sqrtf(playerToPickupVector.x * playerToPickupVector.x + playerToPickupVector.y * playerToPickupVector.y) < .333f)
	{
		// Reset the timer
		_timer = _timeLimit;

		// Choose a new post-processing shader to use
		GameEffect newEffect;
		do
		{
			newEffect = (GameEffect)(rand() % 4 + 1);
		} while (newEffect == _effect);
		_effect = newEffect;

		// Get the angle from the origin to the player
		float angleFromOrigin = atan2(_player->GetTranslation().x, _player->GetTranslation().y) * 180 / 3.14159f;

		// Convert negative angles to be positive
		if (angleFromOrigin < 0)
		{
			angleFromOrigin += 360;
		}

		// Grow the system toward the centre of the grid
		int numberOfRotations;
		if (angleFromOrigin >= 315 || angleFromOrigin < 45) numberOfRotations = 1;
		else if (angleFromOrigin >= 45 && angleFromOrigin < 135) numberOfRotations = 2;
		else if (angleFromOrigin >= 135 && angleFromOrigin < 225) numberOfRotations = 3;
		else numberOfRotations = 0;

		// Rebuild the path and reposition the pickup at the end
		D3DXVECTOR3 goalPosition;
		D3DXVECTOR3 playerPosition = _player->GetGridPosition();
		_grid->Build(Utils::Round(playerPosition.x / 3), Utils::Round(playerPosition.y / 3), numberOfRotations, goalPosition);
		_pickup->SetPosition(goalPosition.x, goalPosition.y, goalPosition.z);
	}
}


TileGrid* GameBoard::GetTileGrid() const
{
	return _grid;
}


Player* GameBoard::GetPlayer() const
{
	return _player;
}


ParticleSystem* GameBoard::GetParticleSystem() const
{
	return _pickup;
}


GameBoard::GameEffect GameBoard::GetEffect() const
{
	return _effect;
}


float GameBoard::GetTimer() const
{
	return _timer;
}


void GameBoard::Reset()
{
	// Reset the timer
	_timer = _timeLimit;

	// Reset the effect
	_effect = None;
	
	// Centre the player
	D3DXVECTOR3 playerPosition = _player->GetTranslation();
	_player->AddTranslation(-playerPosition.x, -playerPosition.y, 0);

	// Clear the maze
	_grid->Erase();

	// Rebuild the maze 
	D3DXVECTOR3 goalPosition;
	_grid->Build(_grid->WIDTH / 6, _grid->HEIGHT / 6, 0, goalPosition);
	_pickup->SetPosition(goalPosition.x, goalPosition.y, goalPosition.z);

}