#ifndef _GAMEBOARD_H_
#define _GAMEBOARD_H_

// Includes
#include "Input.h"
#include "TileGrid.h"
#include "Player.h"
#include "ParticleSystem.h"

class GameBoard
{
public:
	enum GameEffect
	{
		None,
		Blur,
		Edge,
		DoubleVision,
		Pixelate,
		Split,
	};

	GameBoard(ID3D11Device*, Input*);
	~GameBoard();

	void Frame(float, ID3D11DeviceContext*);

	TileGrid* GetTileGrid() const;
	Player* GetPlayer() const;
	ParticleSystem* GetParticleSystem() const;
	GameEffect GetEffect() const;
	float GetTimer() const;
	
	void Reset();

private:
	Input* _input;

	TileGrid* _grid;
	Player* _player;
	ParticleSystem* _pickup;
	GameEffect _effect;

	float _timer;
	float _timeLimit;
};

#endif