#include "Font.h"

Font::Font(ID3D11Device* device, char* fontFilename, WCHAR* textureFilename) :
	_font(0),
	_texture(0)
{
	// Load in the text file containing the font data.
	LoadFontData(fontFilename);

	// Load the texture that has the font characters on it.
	LoadTexture(device, textureFilename);
}

Font::Font(const Font& other)
{
}

Font::~Font()
{
	// Release the font texture.
	ReleaseTexture();

	// Release the font data.
	ReleaseFontData();
}


bool Font::LoadFontData(char* filename)
{
	ifstream fin;
	int i;
	char temp;

	// Create the font spacing buffer.
	_font = new FontType[95];
	if(!_font)
		return false;

	// Read in the font size and spacing between chars.
	fin.open(filename);
	if(fin.fail())
		return false;

	// Read in the 95 used ascii characters for text.
	for(i=0; i<95; i++)
	{
		fin.get(temp);
		while(temp != ' ')
			fin.get(temp);

		fin.get(temp);
		while(temp != ' ')
			fin.get(temp);

		fin >> _font[i].left;
		fin >> _font[i].right;
		fin >> _font[i].size;
	}

	// Close the file.
	fin.close();

	return true;
}

void Font::ReleaseFontData()
{
	// Release the font data array.
	if(_font)
	{
		delete [] _font;
		_font = 0;
	}

	return;
}

void Font::LoadTexture(ID3D11Device* device, WCHAR* filename)
{
	// Create and initialize the texture object.
	_texture = new Texture(device, filename);
}

void Font::ReleaseTexture()
{
	// Release the texture object.
	if(_texture)
	{
		delete _texture;
		_texture = 0;
	}

	return;
}

ID3D11ShaderResourceView* Font::GetTexture()
{
	return _texture->GetTexture();
}

void Font::BuildVertexArray(void* vertices, char* sentence, float drawX, float drawY)
{
	VertexType* vertexPtr;
	int numLetters, index, i, letter;

	// Coerce the input vertices into a VertexType structure.
	vertexPtr = (VertexType*)vertices;

	// Get the number of letters in the sentence.
	numLetters = (int)strlen(sentence);

	// Initialize the index to the vertex array.
	index = 0;

	// Draw each letter onto a quad.
	for(i=0; i<numLetters; i++)
	{
		letter = ((int)sentence[i]) - 32;

		// If the letter is a space then just move over three pixels.
		if(letter == 0)
			drawX = drawX + 3.0f;
		else
		{
			// First triangle in quad.
			vertexPtr[index].position = D3DXVECTOR3(drawX, drawY, 0.0f);  // Top left.
			vertexPtr[index].texture = D3DXVECTOR2(_font[letter].left, 0.0f);
			index++;

			vertexPtr[index].position = D3DXVECTOR3((drawX + _font[letter].size), (drawY - 16), 0.0f);  // Bottom right.
			vertexPtr[index].texture = D3DXVECTOR2(_font[letter].right, 1.0f);
			index++;

			vertexPtr[index].position = D3DXVECTOR3(drawX, (drawY - 16), 0.0f);  // Bottom left.
			vertexPtr[index].texture = D3DXVECTOR2(_font[letter].left, 1.0f);
			index++;

			// Second triangle in quad.
			vertexPtr[index].position = D3DXVECTOR3(drawX, drawY, 0.0f);  // Top left.
			vertexPtr[index].texture = D3DXVECTOR2(_font[letter].left, 0.0f);
			index++;

			vertexPtr[index].position = D3DXVECTOR3(drawX + _font[letter].size, drawY, 0.0f);  // Top right.
			vertexPtr[index].texture = D3DXVECTOR2(_font[letter].right, 0.0f);
			index++;

			vertexPtr[index].position = D3DXVECTOR3((drawX + _font[letter].size), (drawY - 16), 0.0f);  // Bottom right.
			vertexPtr[index].texture = D3DXVECTOR2(_font[letter].right, 1.0f);
			index++;

			// Update the x location for drawing by the size of the letter and one pixel.
			drawX = drawX + _font[letter].size + 1.0f;
		}
	}

	return;
}