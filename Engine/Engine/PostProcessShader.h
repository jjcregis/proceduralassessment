#ifndef _POSTPROCESSSHADER_H_
#define _POSTPROCESSSHADER_H_

// Includes
#include <d3d11.h>
#include <d3dx10math.h>
#include <d3dx11async.h>
#include <fstream>
#include "Shader.h"
using namespace std;

class PostProcessShader : public Shader
{

public:
	PostProcessShader(ID3D11Device*, HWND, bool, WCHAR*, WCHAR*, char*, char*);
	~PostProcessShader();
	bool Render(ID3D11DeviceContext*, int, D3DXMATRIX, D3DXMATRIX, D3DXMATRIX, D3DXMATRIX, ID3D11ShaderResourceView*, D3DXVECTOR2, float);

private:
	bool SetShaderParameters(ID3D11DeviceContext*, D3DXMATRIX, D3DXMATRIX, D3DXMATRIX, D3DXMATRIX, ID3D11ShaderResourceView*, D3DXVECTOR2, float);

private:
	ID3D11Buffer* _screenSizeBuffer;
};

#endif