#define _USE_MATH_DEFINES

#include "TileGrid.h"
#include "Tile.h"
#include <math.h>

Tile::Tile(ID3D11Device* device, TileGrid* grid, int type, int x, int y) :
	_vertexBuffer(0),
	_indexBuffer(0),
	_model(0),
	_textureArray(0),
	_rotation(0),
	_rotationRate(.012f),
	_isFlipping(false),
	_nextType(0),
	_grid(grid),
	_type(type),
	_gridPosition(D3DXVECTOR2((float)x, (float)y))
{
	// Get the texture associated with the tile type
	_textureArray = _grid->GetTexture(type);

	// Load in the model data
	BuildTileGeometry();

	// Initialize the vertex and index buffers.
	InitializeBuffers(device);
}


Tile::Tile(const Tile& other)
{
}


Tile::~Tile()
{
	// Shutdown the vertex and index buffers.
	ShutdownBuffers();

	// Release the model data.
	ReleaseModel();
}


void Tile::Render(ID3D11DeviceContext* deviceContext)
{
	// Put the vertex and index buffers on the graphics pipeline to prepare them for drawing.
	RenderBuffers(deviceContext);

	return;
}


void Tile::Frame(float frameTime)
{
	if (_isFlipping)
	{
		_rotation += frameTime * _rotationRate;
		
		// Once the tile is facing backwards, change its texture
		if (_rotation > M_PI * 2 / 3 && _rotation < M_PI)
		{
			_rotation += (float)(M_PI * 2 / 3);
			SetType(_nextType);
		}
	}

	if (_rotation >= M_PI * 2)
	{
		_isFlipping = false;
		_rotation = 0;
	}
}


int Tile::GetIndexCount()
{
	return _indexCount;
}


ID3D11ShaderResourceView** Tile::GetTextureArray()
{
	return _textureArray->GetTextureArray();
}


ID3D11ShaderResourceView* Tile::GetPrimaryTexture()
{
	return _textureArray->GetTextureArray()[0];
}


ID3D11ShaderResourceView* Tile::GetSecondaryTexture()
{
	return _textureArray->GetTextureArray()[1];
}


D3DXMATRIX Tile::GetTranslationMatrix()
{
	// Transform the tile to its position in the grid, and centre the grid on the origin
	D3DXMATRIX tileOffset;
	D3DXMATRIX gridOffset;
	D3DXMATRIX tileRotation;
	D3DXMATRIX finalTransform;

	// Apply any rotation to the tile
	D3DXMatrixRotationX(&tileRotation, _rotation);

	// Translate to its position in the grid
	D3DXMatrixTranslation(&tileOffset, _gridPosition.x, _gridPosition.y, 0);

	// Offset the grid piece to situate the grid in the centre of the screen
	D3DXMatrixTranslation(&gridOffset, -_grid->WIDTH / 2.0f + .5f, -_grid->HEIGHT / 2.0f + .5f, 0);

	// Multiply to get the final transform
	D3DXMatrixIdentity(&finalTransform);
	D3DXMatrixMultiply(&finalTransform, &finalTransform, &tileRotation);
	D3DXMatrixMultiply(&finalTransform, &finalTransform, &tileOffset);
	D3DXMatrixMultiply(&finalTransform, &finalTransform, &gridOffset);

	return finalTransform;
}


int Tile::GetType()
{
	return _type;
}


void Tile::SetType(int type)
{
	if (_type != type)
	{
		_type = type;

		// Get the new texture for this tile
		_textureArray = _grid->GetTexture(type);
	}
}


void Tile::FlipSet(int type)
{
	if (!_isFlipping)
	{
		_rotation = 0;
		_nextType = type;
		_isFlipping = true;
	}
}


bool Tile::InitializeBuffers(ID3D11Device* device)
{
	VertexType* vertices;
	unsigned long* indices;
	D3D11_BUFFER_DESC vertexBufferDesc, indexBufferDesc;
	D3D11_SUBRESOURCE_DATA vertexData, indexData;
	HRESULT result;
	int i;

	// Create the vertex and index arrays.
	vertices = new VertexType[_vertexCount];
	if (!vertices)
		return false;

	indices = new unsigned long[_indexCount];
	if (!indices)
		return false;

	// Load the vertex array and index array with data.
	for (i = 0; i<_vertexCount; i++)
	{
		vertices[i].position = D3DXVECTOR3(_model[i].x, _model[i].y, _model[i].z);
		vertices[i].texture = D3DXVECTOR2(_model[i].tu, _model[i].tv);

		indices[i] = i;
	}

	// Set up the description of the static vertex buffer.
	vertexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	vertexBufferDesc.ByteWidth = sizeof(VertexType)* _vertexCount;
	vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vertexBufferDesc.CPUAccessFlags = 0;
	vertexBufferDesc.MiscFlags = 0;
	vertexBufferDesc.StructureByteStride = 0;

	// Give the subresource structure a pointer to the vertex data.
	vertexData.pSysMem = vertices;
	vertexData.SysMemPitch = 0;
	vertexData.SysMemSlicePitch = 0;

	// Now create the vertex buffer.
	result = device->CreateBuffer(&vertexBufferDesc, &vertexData, &_vertexBuffer);
	if (FAILED(result))
		return false;

	// Set up the description of the static index buffer.
	indexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	indexBufferDesc.ByteWidth = sizeof(unsigned long)* _indexCount;
	indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	indexBufferDesc.CPUAccessFlags = 0;
	indexBufferDesc.MiscFlags = 0;
	indexBufferDesc.StructureByteStride = 0;

	// Give the subresource structure a pointer to the index data.
	indexData.pSysMem = indices;
	indexData.SysMemPitch = 0;
	indexData.SysMemSlicePitch = 0;

	// Create the index buffer.
	result = device->CreateBuffer(&indexBufferDesc, &indexData, &_indexBuffer);
	if (FAILED(result))
		return false;

	// Release the arrays now that the vertex and index buffers have been created and loaded.
	delete[] vertices;
	vertices = 0;

	delete[] indices;
	indices = 0;

	return true;
}


void Tile::ShutdownBuffers()
{
	// Release the index buffer.
	if (_indexBuffer)
	{
		_indexBuffer->Release();
		_indexBuffer = 0;
	}

	// Release the vertex buffer.
	if (_vertexBuffer)
	{
		_vertexBuffer->Release();
		_vertexBuffer = 0;
	}

	return;
}


void Tile::RenderBuffers(ID3D11DeviceContext* deviceContext)
{
	unsigned int stride;
	unsigned int offset;

	// Set vertex buffer stride and offset.
	stride = sizeof(VertexType);
	offset = 0;

	// Set the vertex buffer to active in the input assembler so it can be rendered.
	deviceContext->IASetVertexBuffers(0, 1, &_vertexBuffer, &stride, &offset);

	// Set the index buffer to active in the input assembler so it can be rendered.
	deviceContext->IASetIndexBuffer(_indexBuffer, DXGI_FORMAT_R32_UINT, 0);

	// Set the type of primitive that should be rendered from this vertex buffer, in this case triangles.
	deviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);

	return;
}


bool Tile::BuildTileGeometry()
{
	// Read in the vertex count.
	_vertexCount = 4;

	// Set the number of indices to be the same as the vertex count.
	_indexCount = 4;

	// Create the model using the vertex count that was read in.
	_model = new TileType[_vertexCount];
	if (!_model)
		return false;

	// Vertex data for a square (using triangle strip)
	// Top left
	_model[0].x = -.5f;
	_model[0].y = -.5f;
	_model[0].z = 0;
	_model[0].tu = 0;
	_model[0].tv = 0;

	// Bottom left
	_model[1].x = -.5f;
	_model[1].y = .5f;
	_model[1].z = 0;
	_model[1].tu = 0;
	_model[1].tv = 1;

	// Top right
	_model[2].x = .5f;
	_model[2].y = -.5f;
	_model[2].z = 0;
	_model[2].tu = 1;
	_model[2].tv = 0;

	// Bottom right
	_model[3].x = .5f;
	_model[3].y = .5f;
	_model[3].z = 0;
	_model[3].tu = 1;
	_model[3].tv = 1;

	return true;
}


void Tile::ReleaseModel()
{
	if (_model)
	{
		delete[] _model;
		_model = 0;
	}

	return;
}