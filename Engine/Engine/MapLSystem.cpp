#include "MapLSystem.h"
#include "TileGrid.h"
#include <vector>
#include <stack>
#include <ctime>

MapLSystem::MapLSystem(TileGrid* grid) :
	_grid(grid)
{
}


MapLSystem::MapLSystem(const MapLSystem& other)
{
}


MapLSystem::~MapLSystem()
{
}


void MapLSystem::Generate(TileGrid* grid, int iterations, int x, int y, int initialRotations, D3DXVECTOR3& pickupPosition)
{
	// Build the L-system string
	BuildSystemString(iterations);

	// Generate the map using the string
	ImplementSystemString(grid, x, y, initialRotations, pickupPosition);
}


void MapLSystem::BuildSystemString(int iterations)
{
	// Revert string to initial state
	_systemString = "S";

	// Generate the system
	string tempString;
	char character;
	string characterReplacement;
	int stringPosition;

	for (int i = 0; i < iterations; i++)
	{
		tempString = _systemString;
		stringPosition = 0;

		for (unsigned int j = 0; j < _systemString.length(); j++)
		{
			character = _systemString[j];
			characterReplacement = EvolveCharacter(character);
			
			tempString.replace(stringPosition, 1, characterReplacement);

			stringPosition += characterReplacement.length();
		}

		_systemString = tempString;
	}
}


void MapLSystem::ImplementSystemString(TileGrid* grid, int x, int y, int initialRotations, D3DXVECTOR3& pickupPosition)
{
	// Determine the deepest branch path (this usually gives us the longest path)
	int deepestBranchDepth = 0;
	int currentBranchDepth = 0;
	for (int i = 0; i < _systemString.length(); i++)
	{
		if (_systemString[i] == '[') currentBranchDepth++;
		else if (_systemString[i] == ']') currentBranchDepth--;
		deepestBranchDepth = max(deepestBranchDepth, currentBranchDepth);
	}

	// Parse the string backwards to find the end of the deepest branch
	int pickupPositionInString = 0;
	currentBranchDepth = 0;
	for (int i = _systemString.length() - 1; i > 0; i--)
	{
		if (_systemString[i] == '[') currentBranchDepth--;
		else if (_systemString[i] == ']') currentBranchDepth++;
		if (currentBranchDepth == deepestBranchDepth && _systemString[i] == 'L')
		{
			pickupPositionInString = i;
			break;
		}
	}
	

	// Keep track of 3x3 sections of the grid that have been written to so sections aren't overwritten
	int cellsHeight = grid->HEIGHT / 3;
	int cellsWidth = grid->WIDTH / 3;
	vector<bool> cells = vector<bool>(cellsWidth * cellsHeight);

	// Keep a stack of build orientations to support branching
	stack<BuildState> stateStack;

	// Keep track of builder's position and orientation
	// Start at the far left, at mid-height, facing right
	BuildState state;
	state.X = x;
	state.Y = y;
	state.Rotation = initialRotations;

	// Begin one cell backwards in the build direction so first cell is built at the player's location
	if (state.Rotation == 0) state.X--;
	else if (state.Rotation == 1) state.Y++;
	else if (state.Rotation == 2) state.X++;
	else if (state.Rotation == 3) state.Y--;

	// Set all cells to unvisited
	for (int i = 0; i < cells.size(); i++)
	{
		cells.at(i) = false;
	}

	// Parse the string and generate the grid
	for (unsigned int i = 0; i < _systemString.length(); i++)
	{
		char character = _systemString[i];

		// Set the position and orientation for the map builder's state depending on what type of cell was built
		switch (character)
		{
		case 'L':
			if (state.Rotation == 0) state.X++;
			else if (state.Rotation == 1) state.Y--;
			else if (state.Rotation == 2) state.X--;
			else if (state.Rotation == 3) state.Y++;
			break;
		case 'C':
			if (state.Rotation == 0) state.X++;
			else if (state.Rotation == 1) state.Y--;
			else if (state.Rotation == 2) state.X--;
			else if (state.Rotation == 3) state.Y++;
			break;
		case '[':
			stateStack.push(state);
			break;
		case ']':
			state = stateStack.top();
			stateStack.pop();
			break;
		case '+':
			state.Rotation++;
			break;
		case '-':
			state.Rotation--;
			break;
		default:
			break;
		}

		// Adjust the rotation to lie between [0, 3]
		if (state.Rotation < 0)
		{
			state.Rotation += 4;
		}
		else if (state.Rotation > 3)
		{
			state.Rotation -= 4;
		}

		// Stay in the map's bounds
		if (state.X >= 0 && state.X < cellsWidth &&
			state.Y >= 0 && state.Y < cellsHeight)
		{
			// Only visit if the cell has not been visited yet
			if (!cells[state.Y * cellsWidth + state.X])
			{
				// Get the 3x3 wall/path data for the current character
				bool* content = GetCellTiles(character, state.Rotation, false);

				// Write the wall/path data to the grid
				BuildCell(grid, content, state);

				// Place the pickup in this cell if it is part of the deepest branch
				if (i <= pickupPositionInString)
				{
					// Get the tile index
					int tilePosition = _grid->WIDTH*((state.Y * 3) + 1) + (state.X * 3) + 1;

					// Convert index to X Y coordinates
					D3DXVECTOR3 tileCoordinates = D3DXVECTOR3(tilePosition % _grid->WIDTH, tilePosition / _grid->WIDTH, 0);

					// If the new position is still in the map, move the pickup further down the path. Otherwise, use the last position.
					if (tileCoordinates.x >= 0 &&
						tileCoordinates.x <= _grid->WIDTH &&
						tileCoordinates.y >= 0 &&
						tileCoordinates.y <= _grid->HEIGHT)
					{
						// Offset to place on grid that is centred
						pickupPosition = tileCoordinates + D3DXVECTOR3(-_grid->WIDTH / 2.0f + .5f, -_grid->HEIGHT / 2.0f + .5f, 0);
					}
				}

				// Delete the cell data now that it is in the grid
				delete[] content;
			}
			
			// Mark the cell as visited
			cells[state.Y * cellsWidth + state.X] = true;
		}

	}
}


bool* MapLSystem::GetCellTiles(char character, int rotations, bool decorate)
{
	bool* content = new bool[9];

	// Set all cells to unvisited
	for (int i = 0; i < 9; i++)
	{
		content[i] = false;
	}

	// Populate the array with the paths and walls
	switch (character)
	{
	case 'L':
	case 'S':
		/*
		000
		111
		000
		*/
		content[0] = false;
		content[1] = false;
		content[2] = false;
		content[3] = true;
		content[4] = true;
		content[5] = true;
		content[6] = false;
		content[7] = false;
		content[8] = false;
		break;
	case 'C':
		/*
		010
		111
		010
		*/
		content[0] = false;
		content[1] = true;
		content[2] = false;
		content[3] = true;
		content[4] = true;
		content[5] = true;
		content[6] = false;
		content[7] = true;
		content[8] = false;
	default:
		break;
	}

	// Rotate the cell
	for (int i = 0; i < rotations; i++)
	{
		// Rotate corners
		bool temp = content[0];
		content[0] = content[2];
		content[2] = content[8];
		content[8] = content[6];
		content[6] = temp;

		// Rotate edges
		temp = content[1];
		content[1] = content[5];
		content[5] = content[7];
		content[7] = content[3];
		content[3] = temp;
	}

	return content;
}


void MapLSystem::BuildCell(TileGrid* grid, bool* cellContent, BuildState state)
{
	// Convert cell coordinates to grid coordinates
	int x = state.X * 3;
	int y = state.Y * 3;
	int width = grid->WIDTH;
	int height = grid->HEIGHT;

	//Add content to grid
	for (int i = 0; i < 9; i++)
	{
		if (i < 3)
		{
			// Bottom row
			_grid->GetTile(width*y + x + i)->FlipSet(cellContent[i] ? 1 : 0);
		}
		else if (i < 6)
		{
			// Middle row
			_grid->GetTile(width*(y + 1) + x + (i - 3))->FlipSet(cellContent[i] ? 1 : 0);
		}
		else
		{
			// Top row
			_grid->GetTile(width*(y + 2) + x + (i - 6))->FlipSet(cellContent[i] ? 1 : 0);
		}
	}
}


string MapLSystem::EvolveCharacter(char character)
{
	/*
	Get the evolution of the character according to the system's rules:
	S -> LS
	S -> LSL
	S -> C[+LS]
	S -> C[-LS]

	Symbols stand for (L)ong and (C)ross.
	*/

	string evolution;

	switch (character)
	{
	case 'S':
	{
		switch (RandomInt(5))
		{
		case 0:
		case 1:
			evolution = "LS";
			break;
		case 2:
			evolution = "LSLL";
			break;
		case 3:
			evolution = "C[+LS]";
			break;
		case 4:
			evolution = "C[-LS]";
			break;
		default:
			break;
		}
		break;
	}
	default:
		// Return the same character
		evolution = string(1, character);
		break;
	}

	return evolution;
}


int MapLSystem::RandomInt(int max)
{
	// Return a random int between [0, max - 1]
	return rand() % max;
}