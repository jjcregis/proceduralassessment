#ifndef _LIGHT_H_
#define _LIGHT_H_

// Includes
#include <d3dx10math.h>

class Light
{
public:
	Light();
	Light(const Light&);
	~Light();

	void SetDiffuseColor(float, float, float, float);
	void SetAmbientColor(float, float, float, float);
	void SetDirection(float, float, float);

	D3DXVECTOR4 GetDiffuseColor();
	D3DXVECTOR4 GetAmbientColor();
	D3DXVECTOR3 GetDirection();

	bool Frame(float);

private:
	D3DXVECTOR4 _diffuseColor;
	D3DXVECTOR3 _direction;
	D3DXVECTOR4 _ambientColor;
};

#endif