#ifndef _DEBUGWINDOW_H_
#define _DEBUGWINDOW_H_

// Includes
#include <d3d11.h>
#include <d3dx10math.h>

class DebugWindow
{
private:
	struct VertexType
	{
		D3DXVECTOR3 position;
	    D3DXVECTOR2 texture;
	};

public:
	DebugWindow(ID3D11Device*, int, int, int, int);
	DebugWindow(const DebugWindow&);
	~DebugWindow();

	bool Render(ID3D11DeviceContext*, int, int);

	int GetIndexCount();

private:
	bool InitializeBuffers(ID3D11Device*);
	void ShutdownBuffers();
	bool UpdateBuffers(ID3D11DeviceContext*, int, int);
	void RenderBuffers(ID3D11DeviceContext*);

private:
	ID3D11Buffer *_vertexBuffer, *_indexBuffer;
	int _vertexCount, _indexCount;
	int _screenWidth, _screenHeight;
	int _bitmapWidth, _bitmapHeight;
	int _previousPosX, _previousPosY;
};

#endif