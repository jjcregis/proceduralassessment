#ifndef _CRACK_H_
#define _CRACK_H_

// Includes
#include <d3d11.h>
#include <d3dx10math.h>
#include <fstream>
#include "Model.h"
#include "TextureArray.h"

using namespace std;

class Crack
{
private:
	struct VertexType
	{
		D3DXVECTOR3 position;
		D3DXVECTOR2 texture;
	};

	struct ImageType
	{
		float x, y, z;
		float tu, tv;
	};

public:
	Crack(ID3D11Device*, WCHAR*, float, float, float, float);
	Crack(const Crack&);
	~Crack();

	void Render(ID3D11DeviceContext*);
	void Frame(float);

	int GetIndexCount();
	ID3D11ShaderResourceView** GetTextureArray();
	ID3D11ShaderResourceView* GetPrimaryTexture();
	ID3D11ShaderResourceView* GetSecondaryTexture();
	D3DXMATRIX GetTranslationMatrix();

private:
	bool InitializeBuffers(ID3D11Device*);
	void ShutdownBuffers();
	void RenderBuffers(ID3D11DeviceContext*);

	bool BuildGeometry();
	void ReleaseModel();

private:
	ID3D11Buffer *_vertexBuffer, *_indexBuffer;
	int _vertexCount, _indexCount;
	TextureArray* _textureArray;
	ImageType* _model;

	D3DXVECTOR2 _position;
	float _rotation;
	float _scale;
};

#endif