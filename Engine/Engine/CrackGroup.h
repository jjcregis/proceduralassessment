#ifndef _CRACKGROUP_H_
#define _CRACKGROUP_H_

// Includes
#include <d3d11.h>
#include <d3dx10math.h>
#include <fstream>
#include <vector>
#include "Crack.h"
#include "CrackLSystem.h"

using namespace std;

class CrackGroup
{
public:
	CrackGroup(ID3D11Device*, float);
	~CrackGroup();

	vector<Crack*>* GetCracks();
	ID3D11Device* GetDevice();

private:
	ID3D11Device* _device;
	CrackLSystem* _lSystem;
	vector<Crack*>* _crackList;

};

#endif