#ifndef _CRACKLSYSTEM_H_
#define _CRACKLSYSTEM_H_

// Includes
#include <d3d11.h>
#include <d3dx10math.h>
#include <string>

using namespace std;

class CrackGroup;

class CrackLSystem
{
private:
	struct BuildState
	{
		float X;
		float Y;
		float Rotation;
		float Scale;
	};

public:
	CrackLSystem();
	CrackLSystem(const CrackLSystem&);
	~CrackLSystem();

	void Generate(CrackGroup*, int, float, float, float);

private:
	void BuildSystemString(int);
	void ImplementSystemString(CrackGroup*, float, float, float);
	string EvolveCharacter(char);
	int RandomInt(int);

private:
	string _systemString;
};

#endif