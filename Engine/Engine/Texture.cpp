#include "Texture.h"

Texture::Texture(ID3D11Device* device, WCHAR* filename) :
	_texture(0)
{
	// Load the texture in.
	SetTexture(device, filename);
}


Texture::Texture(const Texture& other)
{
}


void Texture::SetTexture(ID3D11Device* device, WCHAR* filename)
{
	// Release the old texture
	if (_texture)
	{
		_texture->Release();
		_texture = 0;
	}

	// Load the new texture
	D3DX11CreateShaderResourceViewFromFile(device, filename, NULL, NULL, &_texture, NULL);
}


Texture::~Texture()
{
	// Release the texture resource.
	if (_texture)
	{
		_texture->Release();
		_texture = 0;
	}
}


ID3D11ShaderResourceView* Texture::GetTexture()
{
	return _texture;
}