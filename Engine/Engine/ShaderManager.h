#ifndef _SHADERMANAGER_H_
#define _SHADERMANAGER_H_

// Includes
#include "D3DCore.h"
#include "LightShader.h"
#include "TextureShader.h"
#include "MultiTextureShader.h"
#include "BumpMapShader.h"
#include "ParticleShader.h"
#include "DepthShader.h"
#include "PostProcessShader.h"

class ShaderManager
{
public:
	ShaderManager(ID3D11Device*, HWND, bool);
	ShaderManager(const ShaderManager&);
	~ShaderManager();

	bool RenderLightShader(ID3D11DeviceContext*, int, D3DXMATRIX, D3DXMATRIX, D3DXMATRIX, D3DXMATRIX, ID3D11ShaderResourceView*, 
						   D3DXVECTOR3, D3DXVECTOR4, D3DXVECTOR4);
	bool RenderTextureShader(ID3D11DeviceContext*, int, D3DXMATRIX, D3DXMATRIX, D3DXMATRIX, D3DXMATRIX, ID3D11ShaderResourceView*);
	bool RenderMultiTextureShader(ID3D11DeviceContext*, int, D3DXMATRIX, D3DXMATRIX, D3DXMATRIX, D3DXMATRIX, ID3D11ShaderResourceView**);
	bool RenderBumpMapShader(ID3D11DeviceContext*, int, D3DXMATRIX, D3DXMATRIX, D3DXMATRIX, D3DXMATRIX, ID3D11ShaderResourceView**, 
							 D3DXVECTOR3, D3DXVECTOR4, D3DXVECTOR4);
	bool RenderParticleShader(ID3D11DeviceContext*, int, D3DXMATRIX, D3DXMATRIX, D3DXMATRIX, D3DXMATRIX, ID3D11ShaderResourceView*);
	bool RenderDepthShader(ID3D11DeviceContext*, int, D3DXMATRIX, D3DXMATRIX, D3DXMATRIX, D3DXMATRIX);
	bool RenderBlurShader(ID3D11DeviceContext*, int, D3DXMATRIX, D3DXMATRIX, D3DXMATRIX, D3DXMATRIX, ID3D11ShaderResourceView*, D3DXVECTOR2, float);
	bool RenderEdgeShader(ID3D11DeviceContext*, int, D3DXMATRIX, D3DXMATRIX, D3DXMATRIX, D3DXMATRIX, ID3D11ShaderResourceView*, D3DXVECTOR2, float);
	bool RenderDoubleVisionShader(ID3D11DeviceContext*, int, D3DXMATRIX, D3DXMATRIX, D3DXMATRIX, D3DXMATRIX, ID3D11ShaderResourceView*, D3DXVECTOR2, float);
	bool RenderPixelateShader(ID3D11DeviceContext*, int, D3DXMATRIX, D3DXMATRIX, D3DXMATRIX, D3DXMATRIX, ID3D11ShaderResourceView*, D3DXVECTOR2, float);
	bool RenderSplitShader(ID3D11DeviceContext*, int, D3DXMATRIX, D3DXMATRIX, D3DXMATRIX, D3DXMATRIX, ID3D11ShaderResourceView*, D3DXVECTOR2, float);


private:
	LightShader* _lightShader;
	TextureShader* _textureShader;
	MultiTextureShader* _multiTextureShader;
	BumpMapShader* _bumpMapShader;
	ParticleShader* _particleShader;
	DepthShader* _depthShader;

	PostProcessShader* _blurShader;
	PostProcessShader* _edgeShader;
	PostProcessShader* _doubleVisionShader;
	PostProcessShader* _pixelateShader;
	PostProcessShader* _splitShader;
};

#endif