#ifndef _GRAPHICS_H_
#define _GRAPHICS_H_

// Includes
#include "D3DCore.h"
#include "Input.h"
#include "Camera.h"
#include "Model.h"
#include "TileGrid.h"
#include "Light.h"
#include "Text.h"
#include "ParticleSystem.h"
#include "ShaderManager.h"
#include "RenderTexture.h"
#include "DebugWindow.h"
#include "Player.h"
#include "GameBoard.h"
#include "Crack.h"
#include "CrackGroup.h"

using namespace std;

// Globals
const bool FULL_SCREEN = false;
const bool VSYNC_ENABLED = false;
const float SCREEN_DEPTH = 100.0f;
const float SCREEN_NEAR = .1f;

class Graphics
{
public:
	Graphics(Camera*, Input*, int, int, HWND);
	Graphics(const Graphics&);
	~Graphics();

	bool Frame(int, float, float, GameBoard::GameEffect&);
	bool Render();

	void Reset();

	D3DCore* GetD3D();

private:
	bool RenderToTexture();
	bool RenderScene();

private:
	D3DCore* _d3d;
	Input* _input;
	Camera* _camera;
	Light* _light;

	ShaderManager* _shaderManager;
	RenderTexture* _renderTexture;
	DebugWindow* _debugWindow;

	D3DXMATRIX worldMatrix, viewMatrix, projectionMatrix, orthoMatrix;

	Model* _skyBox;
	GameBoard* _board;

	Text* _fpsText;
	Text* _retryText;
};

#endif