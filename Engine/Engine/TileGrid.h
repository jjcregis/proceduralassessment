#ifndef _TILEGRID_H_
#define _TILEGRID_H_

// Includes
#include "Tile.h"
#include "MapLSystem.h"
#include "ShaderManager.h"

class TileGrid
{
public:
	TileGrid(ID3D11Device*);
	~TileGrid();

	void Render(ID3D11DeviceContext*);
	void Frame(float);

	Tile* GetTile(int);
	Tile* GetTile(int, int);
	
	TextureArray* GetTexture(int);

	void Build(int, int, int, D3DXVECTOR3&);
	void Erase();

	int WIDTH;
	int HEIGHT;

private:
	MapLSystem* _lSystem;
	int _size;
	TextureArray** _textures;
	int numberOfTextures;
	Tile** _tiles;
};

#endif