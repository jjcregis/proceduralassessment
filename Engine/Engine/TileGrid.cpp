#include "TileGrid.h"
#include <ctime>

TileGrid::TileGrid(ID3D11Device* device) :
	WIDTH(51),
	HEIGHT(51),
	_size(WIDTH * HEIGHT)
{
	srand(time(0));

	// Create the grid of tiles and zero pointers
	_tiles = new Tile*[_size];
	for (int i = 0; i < _size; i++)
	{
		_tiles[i] = 0;
	}
	
	// Load the tile textures
	numberOfTextures = 3;
	_textures = new TextureArray*[numberOfTextures];
	_textures[0] = new TextureArray(device, L"../Engine/data/OffBlack.png", NULL);
	_textures[1] = new TextureArray(device, L"../Engine/data/OffWhite.png", NULL);
	_textures[2] = new TextureArray(device, L"../Engine/data/OffRed.png", NULL);

	for (int i = 0; i < _size; i++)
	{
		_tiles[i] = new Tile(device, this, 2, i%WIDTH, i/WIDTH);
	}

	// Generate the L-system map
	_lSystem = new MapLSystem(this);
}


TileGrid::~TileGrid()
{
	// Release the textures
	for (int i = 0; i < numberOfTextures; i++)
	{
		if (_textures[i])
		{
			delete _textures[i];
			_textures[i] = 0;
		}
	}

	// Delete the texture array
	delete[] _textures;

	// Delete the tiles
	for (int i = 0; i < _size; i++)
	{
		if (_tiles[i])
		{
			delete _tiles[i];
			_tiles[i] = 0;
		}
	}

	// Delete the tile array
	delete[] _tiles;

	// Delete the L-System
	delete _lSystem;
}


void TileGrid::Render(ID3D11DeviceContext* deviceContext)
{
	// Render all tiles
	for (int i = 0; i < WIDTH * HEIGHT; i++)
	{
		_tiles[i]->Render(deviceContext);
	}
}


void TileGrid::Frame(float frameTime)
{
	// Update all tiles
	for (int i = 0; i < WIDTH * HEIGHT; i++)
	{
		_tiles[i]->Frame(frameTime);
	}
}


Tile* TileGrid::GetTile(int i)
{
	return _tiles[i];
}


Tile* TileGrid::GetTile(int x, int y)
{
	return _tiles[y*WIDTH + x];
}


TextureArray* TileGrid::GetTexture(int index)
{
	return _textures[index];
}


void TileGrid::Build(int fromX, int fromY, int initialRotations, D3DXVECTOR3& goalPosition)
{
	_lSystem->Generate(this, 5, fromX, fromY, initialRotations, goalPosition);
}

void TileGrid::Erase()
{
	for (int i = 0; i < WIDTH * HEIGHT; i++)
	{
		_tiles[i]->SetType(2);
	}
}