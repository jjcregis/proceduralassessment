#define _USE_MATH_DEFINES

#include "Crack.h"
#include <math.h>

Crack::Crack(ID3D11Device* device, WCHAR* primaryFilename, float x, float y, float rotation, float scale) :
_vertexBuffer(0),
_indexBuffer(0),
_model(0),
_textureArray(0),
_rotation(rotation),
_scale(scale),
_position(D3DXVECTOR2(x, y))
{
	// Load in the model data
	BuildGeometry();

	// Initialize the vertex and index buffers.
	InitializeBuffers(device);

	//Create and initialize the texture array
	_textureArray = new TextureArray(device, primaryFilename, NULL);
}


Crack::Crack(const Crack& other)
{
}


Crack::~Crack()
{
	// Shutdown the vertex and index buffers.
	ShutdownBuffers();

	// Release the model data.
	ReleaseModel();
}


void Crack::Render(ID3D11DeviceContext* deviceContext)
{
	// Put the vertex and index buffers on the graphics pipeline to prepare them for drawing.
	RenderBuffers(deviceContext);

	return;
}


void Crack::Frame(float frameTime)
{
	/*
	if (_isFlipping)
	{
		_rotation += frameTime * _rotationRate;

		// Once the tile is facing backwards, change its texture
		if (_rotation > M_PI * 2 / 3 && _rotation < M_PI)
		{
			_rotation += (float)(M_PI * 2 / 3);
			SetType(_nextType);
		}
	}

	if (_rotation >= M_PI * 2)
	{
		_isFlipping = false;
		_rotation = 0;
	}
	*/
}


int Crack::GetIndexCount()
{
	return _indexCount;
}


ID3D11ShaderResourceView** Crack::GetTextureArray()
{
	return _textureArray->GetTextureArray();
}


ID3D11ShaderResourceView* Crack::GetPrimaryTexture()
{
	return _textureArray->GetTextureArray()[0];
}


ID3D11ShaderResourceView* Crack::GetSecondaryTexture()
{
	return _textureArray->GetTextureArray()[1];
}


D3DXMATRIX Crack::GetTranslationMatrix()
{
	// Transformation matrices
	D3DXMATRIX modelScale;
	D3DXMATRIX modelRotation;
	D3DXMATRIX modelTranslation;
	D3DXMATRIX finalTransform;

	// Rotate the model
	D3DXMatrixRotationZ(&modelRotation, _rotation);

	// Scale the model
	D3DXMatrixScaling(&modelScale, _scale, _scale, _scale);

	// Translate the model
	D3DXMatrixTranslation(&modelTranslation, _position.x, _position.y, 0);

	// Multiply to get the final transform
	D3DXMatrixIdentity(&finalTransform);
	D3DXMatrixMultiply(&finalTransform, &finalTransform, &modelRotation);
	D3DXMatrixMultiply(&finalTransform, &finalTransform, &modelScale);
	D3DXMatrixMultiply(&finalTransform, &finalTransform, &modelTranslation);

	return finalTransform;
}


bool Crack::InitializeBuffers(ID3D11Device* device)
{
	VertexType* vertices;
	unsigned long* indices;
	D3D11_BUFFER_DESC vertexBufferDesc, indexBufferDesc;
	D3D11_SUBRESOURCE_DATA vertexData, indexData;
	HRESULT result;
	int i;

	// Create the vertex and index arrays.
	vertices = new VertexType[_vertexCount];
	if (!vertices)
		return false;

	indices = new unsigned long[_indexCount];
	if (!indices)
		return false;

	// Load the vertex array and index array with data.
	for (i = 0; i<_vertexCount; i++)
	{
		vertices[i].position = D3DXVECTOR3(_model[i].x, _model[i].y, _model[i].z);
		vertices[i].texture = D3DXVECTOR2(_model[i].tu, _model[i].tv);

		indices[i] = i;
	}

	// Set up the description of the static vertex buffer.
	vertexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	vertexBufferDesc.ByteWidth = sizeof(VertexType)* _vertexCount;
	vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vertexBufferDesc.CPUAccessFlags = 0;
	vertexBufferDesc.MiscFlags = 0;
	vertexBufferDesc.StructureByteStride = 0;

	// Give the subresource structure a pointer to the vertex data.
	vertexData.pSysMem = vertices;
	vertexData.SysMemPitch = 0;
	vertexData.SysMemSlicePitch = 0;

	// Now create the vertex buffer.
	result = device->CreateBuffer(&vertexBufferDesc, &vertexData, &_vertexBuffer);
	if (FAILED(result))
		return false;

	// Set up the description of the static index buffer.
	indexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	indexBufferDesc.ByteWidth = sizeof(unsigned long)* _indexCount;
	indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	indexBufferDesc.CPUAccessFlags = 0;
	indexBufferDesc.MiscFlags = 0;
	indexBufferDesc.StructureByteStride = 0;

	// Give the subresource structure a pointer to the index data.
	indexData.pSysMem = indices;
	indexData.SysMemPitch = 0;
	indexData.SysMemSlicePitch = 0;

	// Create the index buffer.
	result = device->CreateBuffer(&indexBufferDesc, &indexData, &_indexBuffer);
	if (FAILED(result))
		return false;

	// Release the arrays now that the vertex and index buffers have been created and loaded.
	delete[] vertices;
	vertices = 0;

	delete[] indices;
	indices = 0;

	return true;
}


void Crack::ShutdownBuffers()
{
	// Release the index buffer.
	if (_indexBuffer)
	{
		_indexBuffer->Release();
		_indexBuffer = 0;
	}

	// Release the vertex buffer.
	if (_vertexBuffer)
	{
		_vertexBuffer->Release();
		_vertexBuffer = 0;
	}

	return;
}


void Crack::RenderBuffers(ID3D11DeviceContext* deviceContext)
{
	unsigned int stride;
	unsigned int offset;

	// Set vertex buffer stride and offset.
	stride = sizeof(VertexType);
	offset = 0;

	// Set the vertex buffer to active in the input assembler so it can be rendered.
	deviceContext->IASetVertexBuffers(0, 1, &_vertexBuffer, &stride, &offset);

	// Set the index buffer to active in the input assembler so it can be rendered.
	deviceContext->IASetIndexBuffer(_indexBuffer, DXGI_FORMAT_R32_UINT, 0);

	// Set the type of primitive that should be rendered from this vertex buffer, in this case triangles.
	deviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);

	return;
}


bool Crack::BuildGeometry()
{
	// Read in the vertex count.
	_vertexCount = 4;

	// Set the number of indices to be the same as the vertex count.
	_indexCount = 4;

	// Create the model using the vertex count that was read in.
	_model = new ImageType[_vertexCount];
	if (!_model)
		return false;

	// Vertex data for a square (using triangle strip)
	// Top Right
	_model[0].x = .5f;
	_model[0].y = 4;
	_model[0].z = 0;
	_model[0].tu = 0;
	_model[0].tv = 0;

	// Bottom right
	_model[1].x = .5f;
	_model[1].y = 0;
	_model[1].z = 0;
	_model[1].tu = 0;
	_model[1].tv = 1;

	// Top left
	_model[2].x = -.5f;
	_model[2].y = 4;
	_model[2].z = 0;
	_model[2].tu = 1;
	_model[2].tv = 0;

	// Bottom left
	_model[3].x = -.5f;
	_model[3].y = 0;
	_model[3].z = 0;
	_model[3].tu = 1;
	_model[3].tv = 1;

	return true;
}


void Crack::ReleaseModel()
{
	if (_model)
	{
		delete[] _model;
		_model = 0;
	}

	return;
}