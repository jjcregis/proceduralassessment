#include "Light.h"

Light::Light()
{
}

Light::Light(const Light& other)
{
}

Light::~Light()
{
}

void Light::SetDiffuseColor(float red, float green, float blue, float alpha)
{
	_diffuseColor = D3DXVECTOR4(red, green, blue, alpha);
	return;
}

void Light::SetAmbientColor(float r, float g, float b, float a)
{
	_ambientColor = D3DXVECTOR4(r,g,b,a);
	return;
}
 
void Light::SetDirection(float x, float y, float z)
{
	_direction = D3DXVECTOR3(x, y, z);
	return;
}

D3DXVECTOR4 Light::GetDiffuseColor()
{
	return _diffuseColor;
}

D3DXVECTOR4 Light::GetAmbientColor()
{
	return _ambientColor;
}

D3DXVECTOR3 Light::GetDirection()
{
	return _direction;
}

bool Light::Frame(float totalTime)
{
	// Move the light to demonstrate lighting
	_direction.x = sin(totalTime/3000000);
	D3DXVec3Normalize(&_direction, &_direction);
	return true;
}