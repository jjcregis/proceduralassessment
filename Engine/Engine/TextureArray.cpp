#include "TextureArray.h"

TextureArray::TextureArray(ID3D11Device* device, WCHAR* primaryFilename, WCHAR* secondaryFilename)
{
	_textures[0] = 0;
	_textures[1] = 0;

	// Load the first texture in.
	D3DX11CreateShaderResourceViewFromFile(device, primaryFilename, NULL, NULL, &_textures[0], NULL);

	// Load the second texture in if one is provided
	if (secondaryFilename != NULL)
	{
		D3DX11CreateShaderResourceViewFromFile(device, secondaryFilename, NULL, NULL, &_textures[1], NULL);
	}
}


TextureArray::TextureArray(const TextureArray& other)
{
}


TextureArray::~TextureArray()
{
	// Release the texture resources.
	if (_textures[0])
	{
		_textures[0]->Release();
		_textures[0] = 0;
	}

	if (_textures[1])
	{
		_textures[1]->Release();
		_textures[1] = 0;
	}
}


ID3D11ShaderResourceView** TextureArray::GetTextureArray()
{
	return _textures;
}