#ifndef _MAPLSYSTEM_H_
#define _MAPLSYSTEM_H_

// Includes
#include <d3d11.h>
#include <d3dx10math.h>
#include <string>

class TileGrid;

using namespace std;

class MapLSystem
{
private:
	struct BuildState
	{
		int X;
		int Y;
		int Rotation;
	};

public:
	MapLSystem(TileGrid*);
	MapLSystem(const MapLSystem&);
	~MapLSystem();

	void Generate(TileGrid*, int, int, int, int, D3DXVECTOR3&);

private:
	void BuildSystemString(int);
	void ImplementSystemString(TileGrid*, int, int, int, D3DXVECTOR3&);
	bool* GetCellTiles(char, int, bool);
	void BuildCell(TileGrid*, bool*, BuildState);
	string EvolveCharacter(char);
	int RandomInt(int);

private:
	TileGrid* _grid;
	string _systemString;
};

#endif