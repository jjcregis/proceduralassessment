// Globals
cbuffer MatrixBuffer
{
	matrix worldMatrix;
	matrix viewMatrix;
	matrix projectionMatrix;
	matrix transformationMatrix;
};


cbuffer ScreenSizeBuffer
{
	float2 screenSize;
	float timer;
	float padding;
};


struct VertexInputType
{
    float4 position : POSITION;
    float2 tex : TEXCOORD0;
};


struct PixelInputType
{
    float4 position : SV_POSITION;
	float2 texCoordLeft : TEXCOORD0;
	float2 texCoordRight : TEXCOORD1;
	float2 texCoordDown : TEXCOORD2;
	float2 texCoordUp : TEXCOORD3;
};


PixelInputType DoubleVisionVertexShader(VertexInputType input)
{
    PixelInputType output;
	float texelWidth;
	float texelHeight;

	// Change the position vector to be 4 units for proper matrix calculations.
    input.position.w = 1.0f;

	// Calculate the position of the vertex against the world, view, and projection matrices.
    output.position = mul(input.position, worldMatrix);
	output.position = mul(output.position, transformationMatrix);
    output.position = mul(output.position, viewMatrix);
    output.position = mul(output.position, projectionMatrix);
    
	// Determine the floating point size of a texel for a screen with this specific width.
	texelWidth = 1.0f / screenSize.x;
	texelHeight = 1.0f / screenSize.y;

	// Create UV coordinates for the pixel and its four horizontal neighbors on either side.
	output.texCoordLeft = clamp(input.tex + float2(texelWidth * -1.0f * sin(timer / 1000) * 25, 0.0f), 0, 1);
	output.texCoordRight = clamp(input.tex + float2(texelWidth * 1.0f * -sin(timer / 1000) * 25, 0.0f), 0, 1);
	output.texCoordDown = clamp(input.tex + float2(0.0f, texelWidth * -1.0f * cos(timer / 1000) * 25), 0, 1);
	output.texCoordUp = clamp(input.tex + float2(0.0f, texelWidth * 1.0f * -cos(timer / 1000) * 25), 0, 1);

    return output;
}