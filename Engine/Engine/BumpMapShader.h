#ifndef _BUMPMAPSHADER_H_
#define _BUMPMAPSHADER_H_

// Includes
#include <d3d11.h>
#include <d3dx10math.h>
#include <d3dx11async.h>
#include <fstream>
#include "Shader.h"
using namespace std;

class BumpMapShader : public Shader
{
public:
	BumpMapShader(ID3D11Device*, HWND, bool);
	~BumpMapShader();
	bool Render(ID3D11DeviceContext*, int, D3DXMATRIX, D3DXMATRIX, D3DXMATRIX, D3DXMATRIX, ID3D11ShaderResourceView**, D3DXVECTOR3, 
				D3DXVECTOR4, D3DXVECTOR4);

private:
	bool SetShaderParameters(ID3D11DeviceContext*, D3DXMATRIX, D3DXMATRIX, D3DXMATRIX, D3DXMATRIX, ID3D11ShaderResourceView**, D3DXVECTOR3, 
							 D3DXVECTOR4, D3DXVECTOR4);

private:
	ID3D11Buffer* _lightBuffer;
};

#endif