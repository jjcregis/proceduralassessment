#ifndef _RENDERTEXTURE_H_
#define _RENDERTEXTURE_H_

// Includes
#include <d3d11.h>

class RenderTexture
{
public:
	RenderTexture(ID3D11Device*, int, int);
	RenderTexture(const RenderTexture&);
	~RenderTexture();

	void SetRenderTarget(ID3D11DeviceContext*, ID3D11DepthStencilView*);
	void ClearRenderTarget(ID3D11DeviceContext*, ID3D11DepthStencilView*, float, float, float, float);
	ID3D11ShaderResourceView* GetShaderResourceView();

private:
	ID3D11Texture2D* _renderTargetTexture;
	ID3D11RenderTargetView* _renderTargetView;
	ID3D11ShaderResourceView* _shaderResourceView;
};

#endif