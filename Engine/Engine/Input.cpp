#include "Input.h"
#include "SystemClass.h"
#include "Player.h"
#include "Camera.h"
#include "TileGrid.h"
#include "Utils.h"

Input::Input(HINSTANCE hinstance, HWND hwnd, int screenWidth, int screenHeight) :
	_directInput(0),
	_keyboard(0),
	_mouse(0),
	_screenWidth(screenWidth),
	_screenHeight(screenHeight),
	_mouseX(0),
	_mouseY(0)
{
	// Initialize the main direct input interface.
	DirectInput8Create(hinstance, DIRECTINPUT_VERSION, IID_IDirectInput8, (void**)&_directInput, NULL);

	// Initialize the direct input interface for the keyboard.
	_directInput->CreateDevice(GUID_SysKeyboard, &_keyboard, NULL);

	// Set the data format.  In this case since it is a keyboard we can use the predefined data format.
	_keyboard->SetDataFormat(&c_dfDIKeyboard);

	// Set the cooperative level of the keyboard to not share with other programs.
	_keyboard->SetCooperativeLevel(hwnd, DISCL_FOREGROUND | DISCL_EXCLUSIVE);

	// Now acquire the keyboard.
	_keyboard->Acquire();

	// Initialize the direct input interface for the mouse.
	_directInput->CreateDevice(GUID_SysMouse, &_mouse, NULL);

	// Set the data format for the mouse using the pre-defined mouse data format.
	_mouse->SetDataFormat(&c_dfDIMouse);

	// Set the cooperative level of the mouse to share with other programs.
	_mouse->SetCooperativeLevel(hwnd, DISCL_FOREGROUND | DISCL_NONEXCLUSIVE);

	// Acquire the mouse.
	_mouse->Acquire();
}

Input::Input(const Input& other)
{
}

Input::~Input()
{
	// Release the mouse.
	if (_mouse)
	{
		_mouse->Unacquire();
		_mouse->Release();
		_mouse = 0;
	}

	// Release the keyboard.
	if (_keyboard)
	{
		_keyboard->Unacquire();
		_keyboard->Release();
		_keyboard = 0;
	}

	// Release the main interface to direct input.
	if (_directInput)
	{
		_directInput->Release();
		_directInput = 0;
	}
}


bool Input::Frame(SystemClass* system)
{
	bool result;

	// Read the current state of the keyboard.
	result = ReadKeyboard();
	if(!result)
		return false;

	// Read the current state of the mouse.
	result = ReadMouse();
	if(!result)
		return false;

	// Process the changes in the mouse and keyboard.
	ProcessInput();

	// Restart on R press
	if (IsKeyPressed(DIK_R))
		system->Restart();

	// Quit on Esc press.
	if(IsKeyPressed(DIK_ESCAPE))
		return false;

	return true;
}


bool Input::Frame(Camera* camera, float frameTime)
{
	//float rotationSpeed = .0003f * frameTime;
	float translationSpeed = .0015f * frameTime;

	// Speed up if shift is held
	if(IsKeyPressed(DIK_LSHIFT) || IsKeyPressed(DIK_RSHIFT))
	{
		//rotationSpeed *= 1.75;
		translationSpeed *= 1.75;
	}

	// Translate the camera
	if(IsKeyPressed(DIK_LEFT))
		camera->Translate(-translationSpeed,0,0);

	if(IsKeyPressed(DIK_RIGHT))
		camera->Translate(translationSpeed,0,0);

	if(IsKeyPressed(DIK_DOWN))
		camera->Translate(0,0,-translationSpeed);

	if(IsKeyPressed(DIK_UP))
		camera->Translate(0,0,translationSpeed);

	return true;
}


bool Input::Frame(Player* player, float frameTime)
{
	float translationSpeed = .004f * frameTime;
	D3DXVECTOR3 translation = D3DXVECTOR3(0, 0, 0);
	
	// Get the player's translation
	if (IsKeyPressed(DIK_A))
		translation += D3DXVECTOR3(-translationSpeed, 0, 0);
	if (IsKeyPressed(DIK_D))
		translation += D3DXVECTOR3(translationSpeed, 0, 0);
	if (IsKeyPressed(DIK_S))
		translation += D3DXVECTOR3(0, -translationSpeed, 0);
	if (IsKeyPressed(DIK_W))
		translation += D3DXVECTOR3(0, translationSpeed, 0);

	// Move the player
	player->AddTranslation(translation.x, translation.y, translation.z);
	
	// Get the player's grid position
	TileGrid* grid = player->GetGrid();
	D3DXVECTOR3 gridPosition = player->GetTranslation();
	gridPosition += D3DXVECTOR3(grid->WIDTH / 2, grid->HEIGHT / 2, 0);
	
	// Only move if the player will walk within the grid and if the player's new position is a walkable tile.
	// Undo the movement if this isn't the case.
	
	Tile* destinationTile = grid->GetTile(Utils::Round(gridPosition.x), Utils::Round(gridPosition.y));		
	if (gridPosition.x + translation.x < 0 ||
		gridPosition.x + translation.x > grid->WIDTH ||
		gridPosition.y + translation.y < 0 ||
		gridPosition.y + translation.y > grid->HEIGHT ||
		(destinationTile != NULL && destinationTile->GetType() != 1))
	{
		player->AddTranslation(-translation.x, -translation.y, -translation.z);
	}
	
	return true;
}


bool Input::ReadKeyboard()
{
	HRESULT result;

	// Read the keyboard device.
	result = _keyboard->GetDeviceState(sizeof(_keyboardState), (LPVOID)&_keyboardState);
	if(FAILED(result))
	{
		// If the keyboard lost focus or was not acquired then try to get control back.
		if((result == DIERR_INPUTLOST) || (result == DIERR_NOTACQUIRED))
		{
			_keyboard->Acquire();
		}
		else
		{
			return false;
		}
	}
		
	return true;
}


bool Input::ReadMouse()
{
	HRESULT result;

	// Read the mouse device.
	result = _mouse->GetDeviceState(sizeof(DIMOUSESTATE), (LPVOID)&_mouseState);
	if(FAILED(result))
	{
		// If the mouse lost focus or was not acquired then try to get control back.
		if((result == DIERR_INPUTLOST) || (result == DIERR_NOTACQUIRED))
			_mouse->Acquire();
		else
			return false;
	}

	return true;
}


void Input::ProcessInput()
{
	// Update the location of the mouse cursor based on the change of the mouse location during the frame.
	_mouseX += _mouseState.lX;
	_mouseY += _mouseState.lY;

	// Ensure the mouse location doesn't exceed the screen width or height.
	if(_mouseX < 0)  { _mouseX = 0; }
	if(_mouseY < 0)  { _mouseY = 0; }
	
	if(_mouseX > _screenWidth)  { _mouseX = _screenWidth; }
	if(_mouseY > _screenHeight) { _mouseY = _screenHeight; }
	
	return;
}


// Returns if the specified key is pressed or not
bool Input::IsKeyPressed(unsigned char keyCode)
{
	// Do a bitwise and on the keyboard state to check if the escape key is currently being pressed.
	return (_keyboardState[keyCode] & 0x80) != '\0';
}


void Input::GetMouseLocation(int& mouseX, int& mouseY)
{
	mouseX = _mouseX;
	mouseY = _mouseY;
	return;
}