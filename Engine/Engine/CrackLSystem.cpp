#include "CrackLSystem.h"
#include "CrackGroup.h"
#include <vector>
#include <stack>
#include <ctime>

CrackLSystem::CrackLSystem()
{
}


CrackLSystem::CrackLSystem(const CrackLSystem& other)
{
}


CrackLSystem::~CrackLSystem()
{
}


void CrackLSystem::Generate(CrackGroup* group, int iterations, float x, float y, float initialRotation)
{
	// Build the L-system string
	BuildSystemString(iterations);

	//Temp
	//_systemString = "S[-S]S[+S]S";

	// Generate the map using the string
	ImplementSystemString(group, x, y, initialRotation);
}


void CrackLSystem::BuildSystemString(int iterations)
{
	// Revert string to initial state
	_systemString = "S";

	// Generate the system
	string tempString;
	char character;
	string characterReplacement;
	int stringPosition;

	for (int i = 0; i < iterations; i++)
	{
		tempString = _systemString;
		stringPosition = 0;

		for (unsigned int j = 0; j < _systemString.length(); j++)
		{
			character = _systemString[j];
			characterReplacement = EvolveCharacter(character);

			tempString.replace(stringPosition, 1, characterReplacement);

			stringPosition += characterReplacement.length();
		}

		_systemString = tempString;
	}
}


void CrackLSystem::ImplementSystemString(CrackGroup* group, float x, float y, float initialRotations)
{	
	// Keep a stack of build orientations to support branching
	stack<BuildState> stateStack;

	float width = 1;
	float height = 4;
	// Keep track of builder's position and orientation
	// Start at the far left, at mid-height, facing right
	BuildState state;
	state.X = x;
	state.Y = y;
	state.Rotation = initialRotations;
	state.Scale = 1;

	// Begin one cell backwards in the build direction so first cell is built at the player's location
	state.X -= sin(state.Rotation) * width * state.Scale;
	state.Y -= cos(state.Rotation) * height * state.Scale;
	

	// Parse the string and generate the grid
	for (unsigned int i = 0; i < _systemString.length(); i++)
	{
		char character = _systemString[i];
		//evolution = "P[-S]P[+S]P";

		// Set the position and orientation for the map builder's state depending on what type of cell was built
		switch (character)
		{
		case 'S':
		case 'P':
			//float scale;
			//scale = stateStack.empty() ? state.Scale : stateStack.top().Scale;

			state.X += sin(state.Rotation) * width * state.Scale;
			state.Y += cos(state.Rotation) * height * state.Scale;
			break;
		case '[':
			state.Scale *= .5f;
			stateStack.push(state);
			break;
		case ']':
			state = stateStack.top();
			state.Scale /= .5f;
			stateStack.pop();
			break;
		case '+':
			state.Rotation += 45 * 3.14159f/180;
			state.X += sin(state.Rotation) * width * state.Scale;
			state.Y += cos(state.Rotation) * height * state.Scale;
			break;
		case '-':
			state.Rotation -= 45 * 3.14159f / 180;
			float rot;
			rot = stateStack.empty() ? state.Rotation : stateStack.top().Rotation;
			state.X += sin(rot) * width * state.Scale;
			state.Y += cos(rot) * height * state.Scale;
			break;
		default:
			break;
		}

		// Adjust the rotation to lie between [0, 360)
		/*
		if (state.Rotation < 0)
		{
			state.Rotation += 2 * 3.14159f;
		}
		else if (state.Rotation >= (2 * 3.14159f))
		{
			state.Rotation -= 2 * 3.14159f;
		}*/

		if (character == 'S' || character == 'P')
		{
			vector<Crack*>* cracks = group->GetCracks();
			cracks->push_back(new Crack(group->GetDevice(), L"../Engine/data/Player.png", state.X, state.Y, state.Rotation, state.Scale));
		}
		
		
		
		// group->A
		/*
		// Stay in the map's bounds
		if (state.X >= 0 && state.X < cellsWidth &&
			state.Y >= 0 && state.Y < cellsHeight)
		{
			// Only visit if the cell has not been visited yet
			if (!cells[state.Y * cellsWidth + state.X])
			{
				// Get the 3x3 wall/path data for the current character
				bool* content = GetCellTiles(character, state.Rotation, false);

				// Write the wall/path data to the grid
				BuildCell(grid, content, state);

				// Place the pickup in this cell if it is part of the deepest branch
				if (i <= pickupPositionInString)
				{
					// Get the tile index
					int tilePosition = _grid->WIDTH*((state.Y * 3) + 1) + (state.X * 3) + 1;

					// Convert index to X Y coordinates
					D3DXVECTOR3 tileCoordinates = D3DXVECTOR3(tilePosition % _grid->WIDTH, tilePosition / _grid->WIDTH, 0);

					// If the new position is still in the map, move the pickup further down the path. Otherwise, use the last position.
					if (tileCoordinates.x >= 0 &&
						tileCoordinates.x <= _grid->WIDTH &&
						tileCoordinates.y >= 0 &&
						tileCoordinates.y <= _grid->HEIGHT)
					{
						// Offset to place on grid that is centred
						pickupPosition = tileCoordinates + D3DXVECTOR3(-_grid->WIDTH / 2.0f + .5f, -_grid->HEIGHT / 2.0f + .5f, 0);
					}
				}

				// Delete the cell data now that it is in the grid
				delete[] content;
			}

			// Mark the cell as visited
			cells[state.Y * cellsWidth + state.X] = true;
		}
		*/

	}
}




string CrackLSystem::EvolveCharacter(char character)
{
	/*
	Get the evolution of the character according to the system's rules:
	//S -> S[-S]S[+S]S

	S-> P[-S]P[+S]P
	*/

	string evolution;

	switch (character)
	{
	case 'S':
	{
		evolution = "P[-S]P[+S]P";
		break;
	}
	default:
		// Return the same character
		evolution = string(1, character);
		break;
	}

	return evolution;
}


int CrackLSystem::RandomInt(int max)
{
	// Return a random int between [0, max - 1]
	return rand() % max;
}