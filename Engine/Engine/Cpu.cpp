#include "Cpu.h"

Cpu::Cpu() :
	_canReadCpu(true),
	_cpuUsage(0)
{
	PDH_STATUS status;

	// Create a query object to poll cpu usage.
	status = PdhOpenQuery(NULL, 0, &_queryHandle);
	if (status != ERROR_SUCCESS)
		_canReadCpu = false;

	// Set query object to poll all cpus in the system.
	status = PdhAddCounter(_queryHandle, TEXT("\\Processor(_Total)\\% processor time"), 0, &_counterHandle);
	if (status != ERROR_SUCCESS)
		_canReadCpu = false;

	_lastSampleTime = GetTickCount();
}

Cpu::Cpu(const Cpu& other)
{
}

Cpu::~Cpu()
{
	if (_canReadCpu)
		PdhCloseQuery(_queryHandle);
}


void Cpu::Frame()
{
	PDH_FMT_COUNTERVALUE value; 

	if(_canReadCpu)
	{
		if((_lastSampleTime + 1000) < GetTickCount())
		{
			_lastSampleTime = GetTickCount(); 
			PdhCollectQueryData(_queryHandle);
			PdhGetFormattedCounterValue(_counterHandle, PDH_FMT_LONG, NULL, &value);
			_cpuUsage = value.longValue;
		}
	}

	return;
}

int Cpu::GetCpuPercentage()
{
	int usage;

	if(_canReadCpu)
		usage = (int)_cpuUsage;
	else
		usage = 0;

	return usage;
}