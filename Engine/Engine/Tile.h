#ifndef _TILE_H_
#define _TILE_H_

// Includes
#include <d3d11.h>
#include <d3dx10math.h>
#include <fstream>
#include "TextureArray.h"

class TileGrid;

using namespace std;

class Tile
{
private:
	struct VertexType
	{
		D3DXVECTOR3 position;
		D3DXVECTOR2 texture;
	};

	struct TileType
	{
		float x, y, z;
		float tu, tv;
	};

public:
	Tile(ID3D11Device*, TileGrid*, int, int, int);
	Tile(const Tile&);
	~Tile();

	void Render(ID3D11DeviceContext*);
	void Frame(float);

	int GetIndexCount();
	ID3D11ShaderResourceView** GetTextureArray();
	ID3D11ShaderResourceView* GetPrimaryTexture();
	ID3D11ShaderResourceView* GetSecondaryTexture();
	D3DXMATRIX GetTranslationMatrix();

	int GetType();
	void SetType(int);
	void FlipSet(int);

private:
	bool InitializeBuffers(ID3D11Device*);
	void ShutdownBuffers();
	void RenderBuffers(ID3D11DeviceContext*);

	bool BuildTileGeometry();
	void ReleaseModel();

private:
	TileGrid* _grid;

	int _type;
	ID3D11Buffer *_vertexBuffer, *_indexBuffer;
	int _vertexCount, _indexCount;
	TileType* _model;
	TextureArray* _textureArray;
	
	bool _isFlipping;
	int _nextType;

	float _rotation;
	float _rotationRate;
	D3DXVECTOR2 _gridPosition;
};

#endif